<?php

function webmail_plus_user_inbox_form() {

	global $user, $webmail_plus_config, $mail_storage;


	if(!$_SESSION['webmail_plus_mail_storage_connected']) drupal_goto('webmail_plus/misconfigured');



	if($_SESSION['webmail_plus_message_sent']!="") {
		drupal_set_message(t('Your message %subject was sent successfully.', $_SESSION['webmail_plus_message_sent']));
		unset($_SESSION['webmail_plus_message_sent']);
	}


	if(preg_match("/(asc|desc)/", $_GET['sort'])) {
		$sort_order = $_GET['sort'];
	} elseif ($webmail_plus_config["user_sort_order"]) {
		$sort_order = $webmail_plus_config["user_sort_order"];
	}

	if(preg_match("/[0-9a-z]{1,32}/i", $_GET['order'])) {
		$sort_by=$_GET['order'];
	} elseif ($webmail_plus_config["user_sort_by"]) {
		$sort_by = $webmail_plus_config["user_sort_by"];
	}

	// attachments is a special case
	if(strtolower($sort_by)=='att') {
		$sort_by='attachments';
	}

	// update user sort preferences
	if($sort_order && $sort_by) {
		// only updateuser table when necessary
		if ($_SESSION['webmail_plus_user_sort_by'] != $sort_by || $_SESSION['webmail_plus_user_sort_order'] != $sort_order) {
			$_SESSION['webmail_plus_user_sort_by'] = $sort_by;
			$_SESSION['webmail_plus_user_sort_order'] = $sort_order;
			db_query("UPDATE webmail_plus_user SET sort_by='%s', sort_order='%s' WHERE uid=%d", $sort_by, $sort_order, $user->uid);
		}
	}

	// if we end up on this page we need to lose all email data for compose page
	$flush_attachments = __webmail_plus_flush_attachments();
	$flush_email = __webmail_plus_flush_this_email();

	$folder = arg(2);

	if(empty($folder)) {
		$folder = "INBOX";
	}

	$_SESSION['webmail_plus_active_folder'] = $folder;

	drupal_set_title('Folder: '.$folder);

	// rebuild folders array
	/*
	$folders_raw = __webmail_plus_get_folders();
	if (sizeof($folders_raw) > 0) {
	  foreach ($folders_raw as $index => $folder_name) {
	    $folders[$folder_name]=$folder_name;
	  }
	}
	*/
	
	$folders = __webmail_plus_get_folders();

	
	$form = array();
	$form['#theme'] = 'webmail_plus_user_inbox_form';
	$form['inbox']['#tree'] = true;

	$form['op'] = array(
		'#type' => 'hidden'
	);	

	// create control buttons that are located on top
	$form['top_buttons']['#tree'] = true;

	$form['top_buttons']['compose'] = array(
		'#value' => t('Compose'),
		'#type' => 'submit',
		'#attributes' => array(
			'onclick' => "return webmail_plusInboxOpCompose()"
		)
	);

	$form['top_buttons']['move'] = array(
		'#type' => 'submit', 
		'#value' => t('Move To'),
		'#attributes' => array(
			'onclick' => "return webmail_plusInboxOpMoveTo()"
		)
	);		
		
	
	$form['top_buttons']['moveto_folder'] = array(
		'#type' => 'select',
		'#options' => $folders
	);

	//		'#default_value' => $_SESSION['webmail_plus_active_folder'],

	
	$form['top_buttons']['mark_unread'] = array(
		'#value' => t('Mark Unread'), 
		'#type' => 'submit',
		'#attributes' => array(
			'onclick' => "return webmail_plusInboxOpMarkUnread()"
		)
	);
	
	$form['top_buttons']['mark_read'] = array(
		'#value' => t('Mark Read'), 
		'#type' => 'submit',
		'#attributes' => array(
			'onclick' => "return webmail_plusInboxOpMarkRead()"
		)		
	);

	// if the active folder is Trash, show the Empty Trash button
	if($_SESSION['webmail_plus_active_folder']=='Trash') {
		
		$form['top_buttons']['empty_trash'] = array(
			'#value' => t('Empty Trash'), 
			'#type' => 'submit',
			'#attributes' => array(
				'onclick' => "return webmail_plusInboxOpEmptyTrash()"
			)
		);
			
	} else {
		
		$form['top_buttons']['delete'] = array(
			'#value' => t('Delete'), 
			'#type' => 'submit',
			'#attributes' => array(
				'onclick' => "return webmail_plusInboxOpDelete()"
			)			
		);
	}

    $display = is_null($_SESSION['webmail_plus_search_display']) ? "ALL" : $_SESSION['webmail_plus_search_display'];

    $form['top_buttons']['display'] = array(
    	'#type' => 'select',
        '#title' => t('Search in'),
        '#options' => array('ALL' => t('All'), 'UNSEEN' => t('Unread'), 'SEEN' => t('Read')),
        '#default_value' => $display
	);

    $form['top_buttons']['search_criteria'] = array(
    	'#type' => 'textfield', 
    	'#title' => t('for'), 
    	'#size' => 20, 
    	'#maxlength' => 255,
	    '#default_value' => $_SESSION['webmail_plus_search_criteria']

    );
	
    
    $form['top_buttons']['search'] = array(
    	'#value' => 'Search', 
    	'#type' => 'submit',
		'#attributes' => array(
			'onclick' => "return webmail_plusInboxOpSearch()"
		)	    	
    );

    if ($_SESSION['webmail_plus_search_results']) {
    	$form['top_buttons']['show_all'] = array(
    		'#value' => t('Show All'), 
    		'#type' => 'submit',
    		'#attributes' => array(
				'onclick' => "return webmail_plusInboxOpShowAll()"
			)	
    	);
        drupal_set_title('Search Results: '.$folder);
    }

	// now handle the remainder of the form

	// flush the header cache if it's time

	$resolved_folder = __webmail_plus_expand_folder($folder);
	
	//echo "resolved folder: ".$resolved_folder;
	
	$reload_cache = FALSE;



	if(!isset($_SESSION['webmail_plus_last_imap_query'][$folder])) {
			//echo "forcing cache reload";
			$reload_cache=TRUE;
	}


	$offset = time()-$_SESSION['webmail_plus_last_imap_query'][$folder];


	if( ($offset>$webmail_plus_config['query_limit'] || $_SESSION['webmail_plus_last_imap_query'][$folder]=="") && $_SESSION['webmail_plus_mail_storage_connected']) {

		$status = $mail_storage -> get_status(__webmail_plus_expand_folder($_SESSION['webmail_plus_active_folder']));

		$current = db_result(db_query("SELECT COUNT(*) FROM {webmail_plus_user_headers} WHERE uid=%d AND folder='%s'", $user->uid, $folder));


		if ( (int) $status->recent >0 || $current != (int) $status->messages || $status->unseen != $unseen) {
			$reload_cache=TRUE;
		}

		$_SESSION['webmail_plus_last_imap_query'][$folder]=time();
	}



	if( $reload_cache && $_SESSION['webmail_plus_mail_storage_connected']) {



			__webmail_plus_clear_folder_cache($folder);


			$mail_storage -> active_folder(__webmail_plus_expand_folder($folder));
			$overview = $mail_storage -> get_overview();


			$_SESSION['webmail_plus_last_imap_query'][$folder]=time();



			$reload_cache=TRUE;

	}



	


	if($overview!="" && $reload_cache) {
		// flush the table since we'll be inserting contents all over again


		//echo "reloading everything<br>";


		foreach($overview as $index=>$message) {
			
			//__webmail_plus_dump($message);
			
			//$attachments = $mail_storage -> get_attachment_overview($message->msgno);
			$attachments = $mail_storage -> get_attachment_files($message->msgno);

			if(empty($attachments)) {
				$attachment_no = 0;
			} else {
				$attachment_no = sizeof($attachments);
			}


			// add the message to the db table so we can sort it
			$rs=db_query("INSERT INTO webmail_plus_user_headers
			(
			id,
			uid,
			folder,
			`date`,
			message_id,
			size,
			`to`,
			`from`,
			cc,
			subject,
			status,
			attachments,
			recent,
			flagged,
			answered,
			deleted,
			seen,
			draft,
			charset
			)
			VALUES
			(
			%d,
			%d,
			'%s',
			'%s',
			'%s',
			%d,
			'%s',
			'%s',
			'%s',
			'%s',
			%d,
			%d,
			%d,
			%d,
			%d,
			%d,
			%d,
			%d,
			'%s')", $message->msgno, $user->uid, $folder,__webmail_plus_iso_date($message->date),$message->message_id, $message->size, $message->to, $message->from, $message->cc, $message->subject, $messag->status, $attachment_no, $message->recent, $message->flagged, $message->answered, $message->deleted, $message->seen, $message->draft, __webmail_plus_extract_charset($message->subject));


		}
	}


		//echo "active folder is ".$_SESSION['webmail_plus_active_folder']."<br>\n";

        // show appropriate headers
        if ($_SESSION['webmail_plus_active_folder']=='Sent' || $_SESSION['webmail_plus_active_folder']=='Drafts') {
			//echo "showing sent/drafts folder<br>";

            $headers = array(
            	array('data' => '<input type="checkbox" id="webmail_plus_select_all" value="0" onClick="webmail_plusChecked(this);" />'.t('Select')),
                array('data' => t('To'), 'field' => '`to`'),
		  		array('data' => t('Subject'), 'field' => 'subject'),
      			array('data' => t('Date'), 'field' => '`date`'),
       			array('data' => t('Att'), 'field' => 'attachments'),
			);


        } else {

            $headers = array(
		        array('data' => '<input type="checkbox" id="webmail_plus_select_all" value="0" onClick="webmail_plusChecked(this);" />'.t('Select')),
				array('data' => t('From'), 'field' => '`from`'),
				array('data' => t('Subject'), 'field' => 'subject'),
				array('data' => t('Date'), 'field' => '`date`'),
				array('data' => t('Att'), 'field' => 'attachments'),
		    );

        }



		// see if search was used
        if ($_SESSION['webmail_plus_search_keyword']!="") {

        	//echo "keyword: ".$_SESSION['webmail_plus_search_keyword']."<br>\n";

        	//$_SESSION['webmail_plus_search_results']
        	//__webmail_plus_dump($_SESSION['webmail_plus_search_results']);

			$msgs = $_SESSION['webmail_plus_search_results'];

			//echo "size of messages ".sizeof($msgs);

			if (sizeof($msgs)<=0) {

				drupal_set_message(t('Search for %keyword returned no results.', array('%keyword'=>$_SESSION['webmail_plus_search_keyword'])));
				unset($_SESSION['webmail_plus_search_keyword']);

	        	$form['inbox'][0]['id'] = array("#value" => '');
	            $form['inbox'][0]['from'] = array("#value" => $msgs);
	            $form['inbox'][0]['date'] = array("#value" => '');
	            $form['inbox'][0]['subject'] = array("#value" => '');
	            $form['inbox'][0]['attachments'] = array("#value" => '');

			} else {

				$form['inbox']['#tree'] = true;
	            $_SESSION['webmail_plus_search_class'] = TRUE;


				$new_folders = array_keys($_SESSION['webmail_plus_search_results']);

				//__webmail_plus_dump($search_folders);

				$new_sql = "SELECT CONCAT(folder,'_',id) AS `id`,`uid`,`folder`,`date`,`message_id`,`size`,`to`,`from`,`cc`,`subject`,`status`,`attachments`,`recent`,`flagged`,`answered`,`deleted`,`seen`,`draft`,`charset` FROM webmail_plus_user_headers WHERE ";


				foreach($_SESSION['webmail_plus_search_results'] as $folder=>$matches) {

	            	// rearrange matches array
	           		foreach($matches as $key=>$value) {
		            	$new_matches[]=trim($key);
	            	}


					$new_clause[]="(uid=".$user->uid." AND folder='".$folder."' AND id IN(".implode($new_matches,',')."))";



				}

				$clauses = implode(' OR ', $new_clause);

				$new_sql = $new_sql.$clauses;


			}


        } else {

			if(!empty($_SESSION['webmail_plus_user_sort_by']) && !empty($_SESSION['webmail_plus_user_sort_order'])) {

		     	$new_sql = "SELECT * FROM webmail_plus_user_headers WHERE uid=".$user->uid." AND folder='".$folder."' ORDER BY `".strtolower($_SESSION['webmail_plus_user_sort_by'])."` ".$_SESSION['webmail_plus_user_sort_order'];

			} else {

		     	$new_sql = "SELECT * FROM webmail_plus_user_headers WHERE uid=".$user->uid." AND folder='".$folder."' ".tablesort_sql($headers);

			}

        }


        // sql will be empty if there were no search results for the criteria specified
        if (!empty($new_sql)) {
  	  //$messages = pager_query(db_rewrite_sql($sql),$webmail_plus_config['messages_per_page'], 0);
  	  $messages = pager_query($new_sql,$webmail_plus_config['messages_per_page'], 0);


	  $form['inbox']['#tree'] = true;

	  while($data = db_fetch_object($messages)) {
	      //__webmail_plus_dump($data);

	      $form['inbox'][$data->id]['id'] = array("#type" => "checkbox", "#required" => FALSE, "#return_value" => 1);

	      if($_SESSION['webmail_plus_active_folder']=='Sent' || $_SESSION['webmail_plus_active_folder']=='Drafts') {
            $form['inbox'][$data->id]['to'] = array("#value" => __webmail_plus_format_recepient(__webmail_plus_format_address($data->to)));
	      } else {
	      	$form['inbox'][$data->id]['from'] = array("#value" => __webmail_plus_format_address($data->from));
	      }

	      $form['inbox'][$data->id]['folder'] = array("#value" => $data->folder,'#type'=>'hidden');

	      $form['inbox'][$data->id]['subject'] = array("#value" => __webmail_plus_format_subject($data->subject));
	      $form['inbox'][$data->id]['date'] = array("#value" => __webmail_plus_format_date($data->date));
	      $form['inbox'][$data->id]['attachments'] = array("#value" => $data->attachments);
	      $form['inbox'][$data->id]['recent'] = array("#value" => $data->recent,'#type'=>'hidden');
	      $form['inbox'][$data->id]['flagged'] = array("#value" => $data->flagged,'#type'=>'hidden');
	      $form['inbox'][$data->id]['answered'] = array("#value" => $data->answered,'#type'=>'hidden');
	      $form['inbox'][$data->id]['deleted'] = array("#value" => $data->deleted,'#type'=>'hidden');
	      $form['inbox'][$data->id]['seen'] = array("#value" => $data->seen,'#type'=>'hidden');
	      $form['inbox'][$data->id]['draft'] = array("#value" => $data->draft,'#type'=>'hidden');
		  $form['inbox'][$data->id]['charset'] = array("#value" => $data->charset,'#type'=>'hidden');

	  }
        }

	$page .= drupal_get_form('webmail_plus_user_inbox_form', $form);
    $page = str_replace("<p>", "", $page);
    $page = str_replace("</p>", "", $page);
	return $page;
}


// creates a custom look for the inbox form
function theme_webmail_plus_user_inbox_form($form) {

	//if($_SESSION['webmail_plus_search_results']) echo "we have search results";


	$output = form_render($form['top_buttons']);
	unset($form['top_buttons']);

	if ($_SESSION['webmail_plus_active_folder']=='Sent' || $_SESSION['webmail_plus_active_folder']=='Drafts') {
		$headers = array(
			array('data' => '<input type="checkbox" id="webmail_plus_select_all" value="0" onClick="webmail_plusChecked(this);" />', t('Select')),
			array('data' => t('To'), 'field' => 'to'),
			array('data' => t('Subject'), 'field' => 'subject'),
			array('data' => t('Date'), 'field' => '`date`'),
			array('data' => t('Att'), 'field' => 'attachments'),
	   );

	} elseif($_SESSION['webmail_plus_search_results']) {
		$headers =  array(
 	      array('data' => '<input type="checkbox" id="webmail_plus_select_all" value="0" onClick="webmail_plusChecked(this);" />', t('Select')),
 	      array('data' => t('Folder'), 'field' =>'`folder`'),
	      array('data' => t('From'), 'field' => '`from`'),
	      array('data' => t('Subject'), 'field' => 'subject'),
	      array('data' => t('Date'), 'field' => '`date`'),
	      array('data' => t('Att'), 'field' => 'attachments'),
	   );

	} else {
    	$headers = array(
 	      array('data' => '<input type="checkbox" id="webmail_plus_select_all" value="0" onClick="webmail_plusChecked(this);" />', t('Select')),
	      array('data' => t('From'), 'field' => '`from`'),
	      array('data' => t('Subject'), 'field' => 'subject'),
	      array('data' => t('Date'), 'field' => '`date`'),
	      array('data' => t('Att'), 'field' => 'attachments'),
	   );
	}

	$rows = array();
	$row=0;
	$folder = $_SESSION['webmail_plus_active_folder'];
	if(empty($folder)) $folder = 'INBOX';

	foreach (element_children($form['inbox']) as $key) {

				//print_r($form['inbox'][$key]);


                $row++;
				$class = "webmail_plus_row_";
                if ($_SESSION['webmail_plus_search_class']) {
                  $class .= 'search';
                } else {
                  $class .= ($row % 2) ? 'odd' : 'even';
                }
                $class .= ($form['inbox'][$key]['seen']['#value'] == 1) ? '_read' : '_unread';
		$class .= "_message";

		// create table rows out of form elements
		if ($_SESSION['webmail_plus_active_folder']=='Sent') {

		    $rows[] = array(
		    	array('data'=>form_render($form['inbox'][$key]['id']),'class'=>$class),
		    	array('data'=>l(__webmail_plus_extract_name(form_render($form['inbox'][$key]['to'])),'webmail_plus/message_view/'.$folder.'/'.$key), 'class'=>$class),
				array('data'=>l(__webmail_plus_decode(decodeHeader($form['inbox'][$key]['subject']['#value']), $form['inbox'][$key]['charset']['#value']), 'webmail_plus/message_view/'.$folder.'/'.$key,$attributes = array(), $query = NULL, $fragment = NULL, $absolute = FALSE, $html = TRUE),'class'=>$class),
		    	array('data'=>l(form_render($form['inbox'][$key]['date']),'webmail_plus/message_view/'.$folder.'/'.$key), 'class'=>$class),
				array('data'=>form_render($form['inbox'][$key]['attachments']),'class'=>$class),
		    );

		    unset($form['inbox'][$key]['subject']);
		    unset($form['inbox'][$key]['charset']);

		} elseif ($_SESSION['webmail_plus_active_folder']=='Drafts') {

		    $rows[] = array(
		    	array('data'=>form_render($form['inbox'][$key]['id']),'class'=>$class),
		    	array('data'=>l(__webmail_plus_extract_name(form_render($form['inbox'][$key]['to'])),'webmail_plus/compose/draft/'.$folder.'/'.$key), 'class'=>$class),
				array('data'=>l(__webmail_plus_decode(decodeHeader($form['inbox'][$key]['subject']['#value']), $form['inbox'][$key]['charset']['#value']), 'webmail_plus/compose/draft/'.$folder.'/'.$key,$attributes = array(), $query = NULL, $fragment = NULL, $absolute = FALSE, $html = TRUE),'class'=>$class),
		    	array('data'=>l(form_render($form['inbox'][$key]['date']),'webmail_plus/compose/draft/'.$folder.'/'.$key), 'class'=>$class),
				array('data'=>form_render($form['inbox'][$key]['attachments']),'class'=>$class),
		    );

		    unset($form['inbox'][$key]['subject']);
		    unset($form['inbox'][$key]['charset']);

		} elseif ($_SESSION['webmail_plus_search_results']) {

		    $rows[] = array(
		    	array('data'=>form_render($form['inbox'][$key]['id']),'class'=>$class),

		    	array('data'=>l($form['inbox'][$key]['folder']['#value'],'webmail_plus/message_view/'.$form['inbox'][$key]['folder']['#value'].'/'.$key),'class'=>$class),

		    	array('data'=>l(__webmail_plus_extract_name(form_render($form['inbox'][$key]['from'])),'webmail_plus/message_view/'.$form['inbox'][$key]['folder']['#value'].'/'.$key),'class'=>$class),

				array('data'=>l(__webmail_plus_decode(decodeHeader($form['inbox'][$key]['subject']['#value']), $form['inbox'][$key]['charset']['#value']), 'webmail_plus/message_view/'.$form['inbox'][$key]['folder']['#value'].'/'.$key,$attributes = array(), $query = NULL, $fragment = NULL, $absolute = FALSE, $html = TRUE),'class'=>$class),

				array('data'=>l(form_render($form['inbox'][$key]['date']),'webmail_plus/message_view/'.$form['inbox'][$key]['folder']['#value'].'/'.$key),'class'=>$class),

				array('data'=>form_render($form['inbox'][$key]['attachments']),'class'=>$class),
		    );

		    unset($form['inbox'][$key]['subject']);
		    unset($form['inbox'][$key]['charset']);


		} else {

		    $rows[] = array(
		    	array('data'=>form_render($form['inbox'][$key]['id']),'class'=>$class),
		    	array('data'=>l(__webmail_plus_extract_name(form_render($form['inbox'][$key]['from'])),'webmail_plus/message_view/'.$folder.'/'.$key), 'class'=>$class),
				array('data'=>l(__webmail_plus_decode(decodeHeader($form['inbox'][$key]['subject']['#value']), $form['inbox'][$key]['charset']['#value']), 'webmail_plus/message_view/'.$folder.'/'.$key,$attributes = array(), $query = NULL, $fragment = NULL, $absolute = FALSE, $html = TRUE),'class'=>$class),		    	array('data'=>l(form_render($form['inbox'][$key]['date']),'webmail_plus/message_view/'.$folder.'/'.$key), 'class'=>$class),
				array('data'=>form_render($form['inbox'][$key]['attachments']),'class'=>$class),
		    );

		    unset($form['inbox'][$key]['subject']);
		    unset($form['inbox'][$key]['charset']);

		}


	}

    // making sure the search class will no be used again unless in search mode
    $_SESSION['webmail_plus_search_class'] = FALSE;

	$table_attributes = array('width' => '100%');
	$output .= theme('table', $headers, $rows, $table_attributes);
	$output .= theme('pager');
	// now align the bottom select box and buttons
	$output .= form_render($form);

	return $output;
}


function webmail_plus_user_inbox_form_submit($form_id, $form_values) {
	global $user, $webmail_plus_config, $mail_storage;

	
	//__webmail_plus_dump($form_values);
	
	
	$switch = $form_values['op'];

	if($switch=="compose") {
		return "webmail_plus/compose";
	}
		
	if($switch=="moveto") {

		
		foreach($form_values['inbox'] as $msgno=>$msgobj) {

			
			if($msgobj["id"]==1) {
				
				//die(__webmail_plus_resolve_folder_name($form_values['top_buttons']['moveto_folder']));	
				
				
				if (($mail_storage -> move($msgno, $form_values['top_buttons']['moveto_folder']))) {
					drupal_set_message(imap_last_error(), 'error');
				}
			}
		}
			
		$mail_storage -> expunge();

		__webmail_plus_clear_folder_cache($_SESSION['webmail_plus_active_folder']);

		return 'webmail_plus/inbox/' . $_SESSION['webmail_plus_active_folder'];			
		
	}
	
	if($switch=="delete") {
	

		//die(__webmail_plus_resolve_folder_name("Trash"));
		
		foreach($form_values['inbox'] as $msgno=>$msgobj) {

			
			if($msgobj["id"]==1) {

				
				if (($mail_storage -> move($msgno, __webmail_plus_expand_folder("Trash")))) {
					drupal_set_message(imap_last_error(), 'error');
				}
			}
		}
			
		$mail_storage -> expunge();

		__webmail_plus_clear_folder_cache('Trash');
		__webmail_plus_clear_folder_cache($_SESSION['webmail_plus_active_folder']);


		return 'webmail_plus/inbox/' . $_SESSION['webmail_plus_active_folder'];			
			
	}
		
	
	if($switch == "markread") {
		

		
		foreach($form_values['inbox'] as $msgno=>$msgobj) {

			
			if($msgobj["id"]==1) {	
				$mail_storage -> set_flag($msgno, '\\Seen');
				$rs=db_query("UPDATE {webmail_plus_user_headers} SET seen=1 WHERE uid=%d AND folder='%s' AND id=%d", $user->uid, $_SESSION['webmail_plus_active_folder'], $msgno);
			}
		}		
		
		return 'webmail_plus/inbox/' . $_SESSION['webmail_plus_active_folder'];
	}
	
	
	if($switch == "markunread") {
		

		
		foreach($form_values['inbox'] as $msgno=>$msgobj) {

			
			if($msgobj["id"]==1) {	
				$mail_storage -> clear_flag($msgno, '\\Seen');
				$rs=db_query("UPDATE {webmail_plus_user_headers} SET seen=0 WHERE uid=%d AND folder='%s' AND id=%d", $user->uid, $_SESSION['webmail_plus_active_folder'], $msgno);
			}
		}		
		
		return 'webmail_plus/inbox/' . $_SESSION['webmail_plus_active_folder'];
	}	
	
	if($switch == "emptytrash") {
		
		$mail_storage -> active_folder(__webmail_plus_expand_folder('Trash'));
		$overview = $mail_storage -> get_overview();

		// iterate thru all the messages in trash
		if (!empty($overview)) {
            	foreach ($overview as $index=>$message) {
	        	$mail_storage -> delete($message->msgno);
	      	}
        		$mail_storage -> expunge();
		}

   	    // delete from cache
   	    db_query("DELETE FROM webmail_plus_user_headers WHERE uid=%d AND folder='%s'", $user->uid, __webmail_plus_expand_folder('Trash'));
		unset($_SESSION['webmail_plus_last_imap_query']['Trash']);
		return 'webmail_plus/inbox/' . $_SESSION['webmail_plus_active_folder'];
					
	}
	
	if($switch == "search") {
		
		//__webmail_plus_dump($form_values);
		
		
		$criteria = array('SUBJECT'); // FIXME: make this a configuration parameter
		$flags = array($form_values['top_buttons']['display']);
   			//__webmail_plus_advanced_search($keywords, $fields = "FROM,SUBJECT,BODY", $folders="INBOX", $flags="ALL");

   		$keywords = $form_values['top_buttons']['search_criteria'];
		$_SESSION['webmail_plus_search_keyword']=$keywords;


		if (!$search_results = __webmail_plus_advanced_search($keywords ,"FROM,SUBJECT,BODY", "INBOX, Sent, Trash")) {
			$msgs = 'No Messages matched your search criteria';
		}


		// storing the values for use when we display the data on screen
		$_SESSION['webmail_plus_search_criteria'] = $form_values['top_buttons']['search_criteria'];
        $_SESSION['webmail_plus_search_display'] = $form_values['top_buttons']['display'];
        $_SESSION['webmail_plus_search_results'] = $search_results;
        drupal_goto('webmail_plus/inbox/' . $_SESSION['webmail_plus_active_folder']);
	}
	
	
	if($switch == "showall") {
            // search is based off these being set, hence show all needs to clear them
            unset($_SESSION['webmail_plus_search_results']);
            unset($_SESSION['webmail_plus_search_display']);
            unset($_SESSION['webmail_plus_search_criteria']);
            unset($_SESSION['webmail_plus_search_keyword']);

            $_SESSION['webmail_plus_search_class'] = FALSE;
            drupal_goto('webmail_plus/inbox/' . $_SESSION['webmail_plus_active_folder']);


	}
}


?>