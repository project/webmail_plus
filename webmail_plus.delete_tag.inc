<?php

/**
 * shows messages by tags
 *
 * @return unknown
 */


// FIXME: need to add tablesort


function webmail_plus_delete_tag() {
  module_load_include('inc', 'webmail_plus', 'webmail_plus.user');
  module_load_include('inc', 'webmail_plus', 'webmail_plus.functions');
  module_load_include('inc', 'webmail_plus', 'webmail_plus.tag_functions');
    

  //drupal_set_message('folder '.arg(0));
  global $user, $_webmail_plus_config;


  
 //echo base_path()."<br>\n";
  $referer = urldecode($_SERVER["HTTP_REFERER"]);
  //echo "referer: $referer<br>\n";
  
 
  if(preg_match("/".WEBMAIL_PLUS_ALIAS."(.*)/", $referer)) {
	  preg_match("/".WEBMAIL_PLUS_ALIAS."(.*)/", $referer, $matches);
	  $redirect = $matches[0];
  }
  else {
  	$redirect = "gateway";
  }  
  
//  print_r($matches);
//  echo "redirect: $redirect<br>\n";
//  die();
  
  $tag = arg(2);

  if($tag) {
    db_query("DELETE FROM {webmail_plus_user_tags} WHERE uid=%d AND tag='%s'", $user->uid, $tag);
  }
  
  drupal_set_message(t('Tag %tag was deleted.', array('%tag'=>$tag)));

  
  drupal_goto($redirect);
}
