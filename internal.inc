<?php
require_once "PEAR.php";
require_once "Mail/RFC822.php";


function __webmail_plus_address_book_lookup($nickname) {
	global $user;

	$nickname = trim(strtolower($nickname));

	$email = db_result(db_query("SELECT email FROM webmail_plus_address_book WHERE uid=%d AND nickname='%s'", $user->uid, $nickname));

	if(empty($email)) {
		return FALSE;
	} else {
		return $email;
	}
}

/*
emails come in many forms.
1. user@domain.com
2. "User Full Name" <user@domain.com>
3. User Full Name <user@domain.com>

this function takes any of those and returns user@domain.com
*/
function __webmail_plus_extract_email($email) {
	$email=trim($email);
	if(empty($email)) return FALSE;

	


	if(strpos($email, "&lt;") || strpos($email, "&gt;")) {
		$email = preg_replace("/\&lt\;/", "<", $email);
		$email = preg_replace("/\&gt\;/", ">", $email);

	}

	// the email is simply user@domain.com
	if(__webmail_plus_email_valid($email)) {
		return $email;
	}
	

	
	//echo "email is question is : $email";

	// email is within <> brackets
	if(preg_match("/(.*)\<(.*)\>/", $email)) {

		//echo "complex email<br>";

		preg_match("/<(.*)>/", $email, $matches);

		
		if(!empty($matches[1])) {

			return $matches[1];
		} else {
			return FALSE;
		}
	}

	return FALSE;

}

/*
takes an email as an input and returns just the user part, removing the domain
*/
function __webmail_plus_extract_user($email) {

	if(!__webmail_plus_email_valid($email)) {
		return FALSE;
	}

	list($username,$domain) = split("@", $email);

	return $username;


}


/*
tries to extract the name from the email
if fails returns the email
*/
function __webmail_plus_extract_name($email) {
	$email = __webmail_plus_present_email($email);
	if(preg_match("/</", $email) && preg_match("/>/", $email)) {
		$name = substr($email,0,strpos($email,"<"));

		if(preg_match("/\"/", $email)) {
			$name = preg_replace("/\"/", "", $name);
			$name = trim($name);

		}

		return $name;

	} else {
		return $email;
	}
}

function __webmail_plus_present_email($email) {
	$email = preg_replace("/\&lt\;/", "<", $email);
	$email = preg_replace("/\&gt\;/", ">", $email);
	return $email;
}


/*
takes input in bytes and returns megabytes, kilobytes
*/
function __webmail_plus_reformat_size($bytes) {
	if($bytes>=1048576) {
		$rs=round($bytes/1048576, 2);
		$units = 'Mb';
	} elseif($bytes>=1024 && $bytes<1048576) {
		$rs = round($bytes/1024, 2);
		$units = 'Kb';
	} else {
		$rs = $bytes;
		$units = 'Bytes';
	}

	return $rs.' '.$units;
}

function __webmail_plus_email_valid($email) {

	if(preg_match("/(.*)\<(.*)\>/", $email)) {
		preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/", $email, $matches);
		$email = $matches[1];

	}
	
	
	
	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$",$email)) {
		return FALSE;
	} else {
		return TRUE;
	}

}


// this does some basic data manipulation to make things easier to read
// if the date is today, it will just return the time, otherwise it will return the date
// in a format Sep 1, 2007
function __webmail_plus_format_date($date) {

	$unixtime = strtotime($date);

	$today = date("Ymd");

	if(date("Ymd",$unixtime)==$today) {

		return date("g:i A", $unixtime);

	} else {

		return date("M j, Y", $unixtime);

	}

}

function __webmail_plus_format_subject($subject) {

	$subject = trim($subject);

	// notify that the message has no subject
	if(empty($subject)) {
		$subject = "(no subject)";
	}

	// trim log subjects
	if(strlen($subject)>65) {
		$subject=substr($subject,0,65)."...";
	}

	return $subject;
}

/*
used in drafts folder view to show the recipient
sometimes an email is stored without a recipient so we need to denote that somehow
if that happens you'll see (no recipient)
*/
function __webmail_plus_format_recepient($recipient) {

	$recipient = trim($recipient);

	if(empty($recipient)) {
		$recipient = "(no recepient)";
	}

	return $recipient;

}

/*
this is used to display the file name or handle it if it's empty
*/
function __webmail_plus_format_filename($subject) {
	global $webmail_plus_config;

	$subject = trim($subject);

	// notify that the message has no subject
	if(empty($subject)) {
		$subject = "(no name)";
	}

	// trim log subjects
	if($webmail_plus_config['inbox_trim_subject']>0 && strlen($subject)>$webmail_plus_config['inbox_trim_subject']) {
		$subject=substr($subject,0,$webmail_plus_config['inbox_trim_subject'])."...";
	}

	return $subject;
}



function __webmail_plus_format_address($email) {

	$email=trim($email);
	if(empty($email)) return "(empty)";

	if(strstr($email, "<") || strstr($email, ">")) {
		$email = str_replace("<", "&lt;", $email);
		$email = str_replace(">", "&gt;", $email);
	}
	return $email;
}

/*
presents any date in an ISO foramt YYY-MM-DD HH:MM:SS
*/
function __webmail_plus_iso_date($date) {
	$unixtime = strtotime($date);

	return date("Y-m-d H:i:s", $unixtime);
}

// returns user password
function __webmail_plus_user_password() {
	global $user;

	$user_password = db_result(db_query('SELECT password FROM {webmail_plus_user} WHERE uid = %d', $user->uid));

	return $user_password;

}

// returns an associative array which contains all the needed webmail_plus configs
function __webmail_plus_get_config() {
	global $user;

	//echo "called get_config<br>\n";

	// general configuration
	$webmail_plus_config['host'] = variable_get('webmail_plus_imap_host', NULL);
	$webmail_plus_config['port'] = variable_get('webmail_plus_imap_port', 143);
	$webmail_plus_config['folder_prefix'] = variable_get('webmail_plus_imap_folder_prefix', 'INBOX');
	$webmail_plus_config['username_append'] = variable_get('webmail_plus_imap_username_append', NULL);
	$webmail_plus_config['query_limit'] = variable_get('webmail_plus_imap_query_limit', 60);
	$webmail_plus_config['options'] = variable_get('webmail_plus_imap_options', NULL);

	$webmail_plus_config['messages_per_page'] = variable_get('webmail_plus_messages_per_page', 25);
	$webmail_plus_config['domain'] = variable_get('webmail_plus_domain',NULL);
	$webmail_plus_config['hide_folders'] = variable_get('webmail_plus_hide_folders', '');
	$webmail_plus_config['required_folders'] = variable_get('webmail_plus_required_folders', 'Drafts, Sent, Trash');
	$webmail_plus_config['can_publish_nodes'] = variable_get('webmail_plus_can_publish_nodes', '');
	$webmail_plus_config['publish_nodes'] = variable_get('webmail_plus_publish_nodes', '');
	$webmail_plus_config['inbox_trim_subject'] = variable_get('webmail_plus_inbox_trim_subject', 0);
	$webmail_plus_config['cache_body'] = variable_get('webmail_plus_cache_body', FALSE);
		
	// delivery thing
	$webmail_plus_config['delivery_method'] = variable_get('webmail_plus_delivery_method', 'localhost');
	$webmail_plus_config['smtp_host'] = variable_get('webmail_plus_smtp_host', 'localhost');


	// fetches LDAP related entries
  	$webmail_plus_config['ldap_address_book'] = variable_get('webmail_plus_ldap_address_book',NULL);
    $webmail_plus_config['ldap_server'] = variable_get('webmail_plus_ldap_server',NULL);
	$webmail_plus_config['ldap_port'] = variable_get('webmail_plus_ldap_port',NULL);
    $webmail_plus_config['ldap_base_dn'] = variable_get('webmail_plus_ldap_base_dn',NULL);
	$webmail_plus_config['ldap_user_attribute'] = variable_get('webmail_plus_ldap_user_attribute',NULL);
	$webmail_plus_config['ldap_search_as_dn'] = variable_get('webmail_plus_ldap_search_as_dn',NULL);
	$webmail_plus_config['ldap_search_as_pass'] = variable_get('webmail_plus_ldap_search_as_pass',NULL);

	// user preferences
	$webmail_plus_config['user_sort_by'] = db_result(db_query("SELECT sort_by FROM webmail_plus_user WHERE uid=%d", $user->uid));
	$webmail_plus_config['user_sort_order'] = db_result(db_query("SELECT sort_order FROM webmail_plus_user WHERE uid=%d", $user->uid));;

	if(empty($webmail_plus_config['user_sort_by'])) $webmail_plus_config['user_sort_by']='Date';
	if(empty($webmail_plus_config['user_sort_order'])) $webmail_plus_config['user_sort_ordar']='desc';

	$webmail_plus_config['user_password'] = __webmail_plus_password(__webmail_plus_user_password(), TRUE);

	return $webmail_plus_config;
}

/*
this returns all nodes which you're allowed to publish an email to
*/
function __webmail_plus_get_publish_nodes() {
	global $webmail_plus_config;

	$available_nodes = node_get_types();
	//print_r($available_nodes);

	/*
	echo "available nodes: ";
	print_r($avaiable_nodes);
	echo "<hr>";
	*/


	$allowed_nodes_arr = explode(",", $webmail_plus_config['publish_nodes']);


	// flip the array
	foreach($allowed_nodes_arr as $index=>$allowed_node_name) {
		$allowed_nodes[trim($allowed_node_name)]=trim($allowed_node_name);
	}
	$good = array_intersect_key($available_nodes, $allowed_nodes);


	foreach($allowed_nodes as $key=>$value) {
		if(node_access('create', $value) && !empty($value) && $available_nodes[$value]) {
			$better[$key]=$value;
		}
	}


	return $better;
}

// fetches a list of folders from the mail storage
// returns an array
function __webmail_plus_get_folders() {
	//echo "getting folders<br>";

	global $webmail_plus_config;

	$folders = __webmail_plus_get_folders_cache();


	// get folders directly form storage and update cache while we're at it
	if(sizeof($folders)<=0) {
		$content .= "folder cache was empty";

		$folders = __webmail_plus_get_folders_storage();
		
		//__webmail_plus_dump($folders);
		
		
		$update = __webmail_plus_put_folders_cache($folders);
	}

	$webmail_plus_config = __webmail_plus_get_config();


	// reformat folders
	if(sizeof($folders)>0) {

		foreach($folders as $index=>$folder) {
			
			if(preg_match('/'.$webmail_plus_config['folder_prefix'].'\./', $folder)) {
				$new[$folder]=substr($folder,strlen($webmail_plus_config['folder_prefix'])+1);
			} else {
				$new[$folder]=$folder;
			}

		}

	}

	$folders = $new;

	// convert hidden folders to a better index
	$hidden_folder_array = explode(',', $webmail_plus_config['hide_folders']);
	foreach($hidden_folder_array as $index=>$folder_name) {
		$hidden_folders[trim($folder_name)]=trim($folder_name);
	}



	// now remove hidden folders from the list
	foreach($folders as $index=>$name) {
		if(!$hidden_folders[$name]) {
			$clean_folders[$index]=$name;
		}
	}
	

	return $clean_folders;
}


// this gets the list of folders from database cache, a lot faster than pulling them from mail storage
function __webmail_plus_get_folders_cache() {
	global $user;

	$result = db_query("SELECT uid,folder FROM {webmail_plus_user_folders} WHERE uid = %d", $user->uid);
	while($data = db_fetch_object($result)) {
	 	$folders[]=$data->folder;
	}
	return $folders;
}

/*
returns a list of folders from the IMAP server
*/
function __webmail_plus_get_folders_storage() {
	global $user, $webmail_plus_config, $mail_storage;

	// this is needed because is the user password isn't set yet
	// the webmail_plus block still calls this function
	// so we need to bail, otherwise it'll cause a fatal error

	if(!is_object($mail_storage)) return FALSE;


	$folders_arr = $mail_storage -> get_folders();
	
	
	
	foreach($folders_arr as $folders_index=>$folder_obj) {
		$folders[]=__webmail_plus_truncate_folder_name($folder_obj->name);
	}

	return $folders;
}

// updates folder cache
function __webmail_plus_put_folders_cache($folders) {
	global $user;

	if(sizeof($folders)<=0) return;

	foreach ($folders as $index => $folder) {
		db_query("INSERT IGNORE INTO {webmail_plus_user_folders} (uid,folder) VALUES(%d,'%s')", $user->uid, $folder);
	}

	return true;
}

// add a folder
function __webmail_plus_add_folder($folder) {
	global $user, $webmail_plus_config, $mail_storage;

	if (!($mail_storage -> add_folder(__webmail_plus_resolve_folder_name($folder)))) {
                drupal_set_message(imap_last_error(), 'error');
        }

	$clear_cache = __webmail_plus_clear_folders_cache();

}

// delete a folder
function __webmail_plus_delete_folder($folder) {
	global $user, $webmail_plus_config, $mail_storage;

	if (!($mail_storage -> delete_folder(__webmail_plus_resolve_folder_name($folder)))) {
                drupal_set_message(imap_last_error(), 'error');
        }

	$clear_cache = __webmail_plus_clear_folders_cache();
}

// rename a folder
function __webmail_plus_rename_folder($oldname, $newname) {
  global $user, $webmail_plus_config, $mail_storage;

  if (!($mail_storage -> rename_folder(__webmail_plus_resolve_folder_name($oldname), __webmail_plus_resolve_folder_name($newname)))) {
    drupal_set_message(imap_last_error(), 'error');
  }

  $clear_cache = __webmail_plus_clear_folders_cache();
}

function __webmail_plus_clear_folders_cache() {
	global $user;

	db_query("DELETE FROM {webmail_plus_user_folders} WHERE uid=%d", $user->uid);

	return TRUE;

}


/*
some IMAP servers store all folders as subfolders of INDEX
this function makes sure that all calls to folders are correct
*/
function __webmail_plus_resolve_folder_name($folder) {
	global $webmail_plus_config;

	// if there's no prefix, just return what we get
	if(empty($webmail_plus_config['folder_prefix'])) {
		return $folder;
	}

	// there's a prefix and it needs special handling
	if($folder=="INBOX") {

		return $folder;

	} else {

		return $webmail_plus_config['folder_prefix'].'.'.$folder;
	}

}

/*
some IMAP servers return folder names in the following format: {localhost:143}INBOX.Drafts
this function strips {localhost:143}
*/
function __webmail_plus_truncate_folder_name($folder) {
	if(preg_match("/\{.*\}/", $folder)) {
		$folder = preg_replace("/\{.*\}/", "", $folder);
	}

	return $folder;
}

/*
cleans up the temp directory
*/
function __webmail_plus_flush_attachments() {
	global $user;

	$tmp_dir = variable_get('file_directory_temp', '/tmp');

	if($_SESSION["attached_files"] && $_SESSION["this_email"]) {

		__webmail_plus_dump($_SESSION["attached_files"]);
		
		foreach($_SESSION["attached_files"] as $attachment_id=>$attachment_filename) {
			
			if(!empty($attachment_filename) && file_exists($tmp_dir."/".$user->name."/".$_SESSION['attached_files'][$attachment_id])) {
				unlink($tmp_dir."/".$user->name."/".$_SESSION['attached_files'][$attachment_id]);
			}
		}

	}

	unset($_SESSION['attached_files']);

}


function __webmail_plus_flush_this_email() {
	unset($_SESSION['this_email']);
}

/*
returns TRUE if an address is in the private address book, retuns FALSE otherwise
*/
function __webmail_plus_address_in_book($email) {

	$email = trim(strtolower($email));

	if(empty($email)) return FALSE;

	$count = db_result(db_query("SELECT COUNT(*) FROM webmail_plus_address_book WHERE email='$email'"));

	if($count>0) {
		return TRUE;
	} else {
		return FALSE;
	}
}

/*
returns TRUE if an address is in LDAP, retuns FALSE otherwise
*/
function __webmail_plus_address_in_ldap($email) {
	$email = trim(strtolower($email));

	if(empty($email)) return FALSE;

}

/*
this uses two hashes to encode/decode passwords
*/
function __webmail_plus_password($string, $decrypt = false) {
	if(empty($string)) return FALSE;

  	return $decrypt ? strtr($string, webmail_plus_HASH_B, webmail_plus_HASH_A) : strtr($string, webmail_plus_HASH_A, webmail_plus_HASH_B);
}

/*
this function decodes the string from a supplied charset
*/
function __webmail_plus_decode($subject, $charset) {

	if(empty($charset)) return $subject;

	$filter_name = strtolower(trim($charset));
	$filter_name = preg_replace("/-/", "_", $filter_name);

	$filter_file = drupal_get_path('module', 'webmail_plus')."/decode/".$filter_name.".php";

	//echo "filter file: $filter_file<br>";

	// load the filter if it exists
	if(file_exists($filter_file)) {
		require_once $filter_file;

		$filter_function = "charset_decode_".$filter_name;

		//echo "filter function $filter_function<br>\n";
		$rs = $filter_function($subject);
		return $rs;


	} else {
		//echo "file $filter_file not found<br>";
		return $subject;
	}

	//echo "filter is $filter_name<br>\n";

}

/*
this function decodes the string from a supplied charset
*/
function __webmail_plus_encode($subject, $charset) {

	if(empty($charset)) return $subject;

	$filter_name = strtolower(trim($charset));
	$filter_name = preg_replace("/-/", "_", $filter_name);

	$filter_file = drupal_get_path('module', 'webmail_plus')."/encode/".$filter_name.".php";

	//echo "filter file: $filter_file<br>";

	// load the filter if it exists
	if(file_exists($filter_file)) {
		require_once $filter_file;

		$filter_function = "charset_encode_".$filter_name;

		//echo "filter function $filter_function<br>\n";
		$rs = $filter_function($subject);
		return $rs;


	} else {
		//echo "file $filter_file not found<br>";
		return $subject;
	}
}

/*
this function tries to guess the charset from a subject
*/
function __webmail_plus_extract_charset($subject) {
	if(empty($subject)) return FALSE;

	if(preg_match('/=\?.*\?[A-Z]\?.*\?=/', $subject)) {

		//echo "there will be a match<br>\n";

		$first_question = strpos($subject, "?");
		$second_question = strpos($subject, "?", $first_question+1);

		//echo "first: $first_question second: $second_question<br>\n";

		$charset = substr($subject, $first_question+1, $second_question-$first_question-1);


		//echo "charset: $charset<br>\n";

		return $charset;

	} else {
		return FALSE;
	}
}

function __webmail_plus_get_profile_field($field) {
	global $user;

	$query = "SELECT profile_values.value FROM profile_values, profile_fields WHERE profile_values.fid=profile_fields.fid AND profile_values.uid=".$user->uid." AND profile_fields.name='".$field."'";

	$result = db_result(db_query($query));

	return $result;

}



function __webmail_plus_clear_folder_cache($folder) {
	global $user;

	$folder = trim($folder);
	if(empty($folder)) return FALSE;
	
	$folder = __webmail_plus_expand_folder($folder);
	
	unset($_SESSION['webmail_plus_last_imap_query'][$folder]);
	db_query("DELETE FROM webmail_plus_user_headers WHERE uid=%d AND folder='%s'", $user->uid, $folder);

}


/*
this function is called from the last message block
it returns a plain english description of how long ago
the last message arrived
*/
function __webmail_plus_last_message_time($datetime) {

	$then = strtotime($datetime);
	$now = time();

	$difference = $now-$then;

	if($difference<=3600) {
		$rs = round($difference/60) . " minutes ago";
	} elseif($difference>3600 && $difference<=86400) {
		$rs = round($difference/3600) . " hours ago";
	} elseif($difference >= 86400 && $difference<604800) {
		$rs = round($difference/86400) . " days ago";
	} else {
		$rs = "over a week ago";
	}

	return $rs;

}

/*
returns TRUE if the user exists, FASLE if doesnt exist
*/

function __webmail_plus_valid_user($username) {

	global $webmail_plus_config;

	// if it's a complete email with a domain that's not local, it's not a user

	/*
	if(__webmail_plus_email_valid($username)) {

		list($username, $domain) = split("@", $username);

		// not a local user
		if($domain!=$webmail_plus_config['domain']) {
			return FALSE;
		}

	}
	*/



	$array = array(
		'name' => $username
	);

	if(user_load($array)) {
		return TRUE;
	} else {
		return FALSE;
	}
}


function __webmail_plus_dump($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}

function __webmail_plus_dump_offset($var) {
	foreach($var as $key=>$value) {
		$var[$key]=time()-$value;
	}

	__webmail_plus_dump($var);
}

function __webmail_plus_make_clickable($text)
{
	# this functions deserves credit to the fine folks at phpbb.com

	$text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);

	// pad it with a space so we can match things at the start of the 1st line.
	$ret = ' ' . $text;

	// matches an "xxxx://yyyy" URL at the start of a line, or after a space.
	// xxxx can only be alpha characters.
	// yyyy is anything up to the first space, newline, comma, double quote or <
	$ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a target=new href=\"\\2\" target=\"_blank\">\\2</a>", $ret);

	// matches a "www|ftp.xxxx.yyyy[/zzzz]" kinda lazy URL thing
	// Must contain at least 2 dots. xxxx contains either alphanum, or "-"
	// zzzz is optional.. will contain everything up to the first space, newline,
	// comma, double quote or <.
	$ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);

	// matches an email@domain type address at the start of a line, or after a space.
	// Note: Only the followed chars are valid; alphanums, "-", "_" and or ".".
	$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"?q=webmail_plus/compose/new/INDEX/0/\\2@\\3\">\\2@\\3</a>", $ret);

	// Remove our padding..
	$ret = substr($ret, 1);
	return $ret;
}

/*
this checks if a particular block is enabled
*/
function __webmail_plus_is_block_enabled($block) {
	$block=trim(strtolower($block));

	if(empty($block)) return FALSE;

	$status = db_result(db_query("SELECT status FROM {blocks} WHERE module='webmail_plus' AND delta='%s' LIMIT 1", $block));

	return $status;
}


/*
recursively searches folders
*/

function __webmail_plus_advanced_search($keywords, $fields = "FROM,SUBJECT,BODY", $folders="INBOX", $flags="ALL") {
	global $mail_storage;

	if(!$mail_storage) return FALSE;

	$keywords = trim($keywords);

	$available_folders = __webmail_plus_get_folders();

	$requested_folders_raw = explode(",", $folders);

	foreach($requested_folders_raw as $key=>$value) {
		if(!empty($value)) $requested_folders[trim($value)]=trim($value);
	}

	$fields_raw = explode(",", $fields);

	foreach($fields_raw as $key=>$value) {
		if(!empty($value)) $requested_fields[]=trim($value);
	}


	$flags_raw=explode(",", $flags);

	foreach($flags_raw as $key=>$value) {
		$requested_flags[]=trim($value);
	}




	$common_folders = array_intersect_key($available_folders, $requested_folders);

	//__webmail_plus_dump($common_folders);

	foreach($common_folders as $folder) {
		//echo $folder."<br>\n";

		foreach($requested_fields as $field) {



			$mail_storage -> active_folder(__webmail_plus_resolve_folder_name($folder));

			// search($query, $criteria, $flags)


			$new_criteria = array($field);

			$rs = $mail_storage -> search($keywords, $new_criteria, $requested_flags); // search($query, $criteria, $flags)




			if($rs=="") continue;

			foreach($rs as $index=>$msgid) {
				$results[$folder][$msgid]=TRUE;
			}



		}

	}

	return $results;

}


/*
A message id (msgid) can be passed just as a number, e.g. 5 or as a number prefixed by folder, e.g. Trash_5
Returns just the number.
*/
function __webmail_plus_extract_msgid($msgid) {

	$msgid = trim($msgid);

	if($msgid=="") return FALSE;

	if(preg_match("/_/", $msgid)) {
		$chunks=preg_split("/_/", $msgid);
		$msgid=$chunks[1];
	}

	return $msgid;
}


/*

takes a string of comma separated address and returns an array of resolved addresses
*/
function __webmail_plus_resolve_addresses($string) {

		global $webmail_plus_config;


		// resolve to fields to complete addresses
		$to_pieces = preg_split("/(,)/", $string);

		foreach($to_pieces as $index=>$address) {

			unset($address_resolved);


			//echo "checking $address<br>";

			// try to resolve the address
			if(__webmail_plus_valid_user($address)) {

				$address_resolved=$address."@".$webmail_plus_config['domain'];

			} elseif (!__webmail_plus_email_valid(__webmail_plus_extract_email($address))) {

				$address_resolved =__webmail_plus_address_book_lookup($address);

			} else {

				$address_resolved = $address;

			}

			//echo "address resolved: $address_resolved<br>\n";

			// need this later when we save the message to Sent
			$message_to_arr[]=$address_resolved;

		}



		return $message_to_arr;
}

/*
attempts to connect to a socket, returns either TRUE or FALSE
used to determine if the IMAP server is actually running
*/
function __webmail_plus_socket_test($host, $port=143, $timeout=30) {

	if(empty($host)) return FALSE;

	$rs = fsockopen($host, $port, $errno, $errstr, $timeout);

	if($rs) {
		fclose($rs);
		return TRUE;
	} else {
		return FALSE;
	}

}

/*
some folder names can be translated, some can not
*/
function __webmail_plus_hidden_folder($folder) {
	global $webmail_plus_config;

	$folder = trim($folder);
	if($folder=="") return FALSE;



	$folder_arr = explode(",", $webmail_plus_config['hide_folders']);


	// cleanup folders
	foreach($folder_arr as $index=>$this_folder) {
		$folder_arr_new[trim($this_folder)]=trim($this_folder);
	}


	if($folder_arr_new[$folder]) {
		return TRUE;
	} else {
		return FALSE;
	}

}

/*
takes a username and returns the uid
*/
function __webmail_plus_lookup_user_id($username) {
	$username=trim($username);

	$uid = db_result(db_query("SELECT uid FROM {users} WHERE name='%s' LIMIT 1", $username));

	if(empty($uid)) return FALSE;

	return $uid;

}

/*
takes a list and returns an array, the returned array can be in 2 different formats:
1. indexed 0=>Hello 1=>World
2. mirrored Hello=>Hello World=>World
*/

function __webmail_plus_list_to_array($list, $mirror=FALSE) {

	if(empty($list)) return FALSE;

	$arr = explode(",", $list);

	foreach($arr as $key=>$value) {

		$value=trim($value);
		if(empty($value)) continue;

		if($mirror) {
			$rs[trim($value)]=trim($value);
		} else {
			$rs[]=trim($value);
		}

	}

	return $rs;
}


/*
this function takes a CSV list of email addresses in all imaginable formats and retuns an array with just user@domain.com
*/

function __webmail_plus_explode_email_list($list) {
	
	if(empty($list)) return FALSE;
	
	$res = Mail_RFC822::parseAddressList($list);
	if (PEAR::isError($res)) {
		return $res->getMessage();
	}
		
	$counter = 0;
	
	foreach($res as $index=>$value) {
		
		unset($addy);
		
		if($value -> personal) $addy = $value -> personal;
		if(!empty($addy)) {
			$addy .= " <".$value -> mailbox."@".$value -> host.">";
		} else {
			$addy = $value -> mailbox."@".$value -> host;
		}

		
		
		$rs[$counter] = $addy;
		
		$counter++;
	}
	
	return $rs;
	
}

/*
takes an email list and returns only good email addresses in it, uses internal address book to resolve @localhost ones
*/
function __webmail_plus_bad_email_list($list) {
	
	$exploded = __webmail_plus_explode_email_list($list);
	
	// handle failed validation that is a result of the explode command
	if(!is_array($exploded) && preg_match("/Validation failed for/", $exploded)) {
		list($warning, $email_address) = split(":", $exploded);
		$email_address=trim($email_address);
		$bad[]=$email_address;
		return $bad;
	}
	
	
	if(sizeof($exploded)<=0) return FALSE;
	
	foreach($exploded as $key=>$email_address) {
		
		//echo "checking: $email_address<br>\n";
		
		if(preg_match("/@localhost/", $email_address)) {
			
			list($username,$domain) = split("@", $email_address);
			
			//echo "checking if $username is in the address book<br>\n";
			
			if(!__webmail_plus_address_book_lookup($username)) {
				//echo "$username isn't in the address book<br>\n";
				$bad[]=$username;
			}
			
		} elseif (!__webmail_plus_email_valid(__webmail_plus_extract_email($email_address))) {
			$bad[]=__webmail_plus_extract_email($email_address);
		}
		
		
	}
	
	//print_r($bad);
	return $bad;
	
	
}

/*
takes a list of emails and returns key-value array like this user@domain -> "User Name" <user@domain>
*/
function __webmail_plus_map_email_list($list) {
	
	if(empty($list)) return FALSE;
	
	$res= Mail_RFC822::parseAddressList($list);
	if (PEAR::isError($res)) return $res->getMessage();
	
	foreach($res as $index=>$value) {
		
		unset($addy);
		
		if($value -> personal) $addy = $value -> personal;
		if(!empty($addy)) {
			$addy .= " <".$value -> mailbox."@".$value -> host.">";
		} else {
			$addy = $value -> mailbox."@".$value -> host;
		}

		
		
		$rs[$value -> mailbox."@".$value -> host] = $addy;

	}

	return $rs;
}

function __webmail_plus_expand_folder($folder) {
	
	$folder = trim($folder);
	if(empty($folder)) return FALSE;
	
	
	$folders_map = array_flip(__webmail_plus_get_folders());
	
	if($folders_map[$folder]) {
		return $folders_map[$folder];
	} else {
		return FALSE;
	}
	
	
}

/*
PHP4 doesn't have htmlspecialchars_decode function so we implement it here
*/
if (!function_exists("htmlspecialchars_decode")) {
	 // "creating htmlspecialchars_decode<br>";t o

    function htmlspecialchars_decode($string, $quote_style = ENT_COMPAT) {
	//die("converting");
        return strtr($string, array_flip(get_html_translation_table(HTML_SPECIALCHARS, $quote_style)));
    }
}




?>