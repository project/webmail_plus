<?php

function webmail_plus_reconnect() {
  drupal_set_title(t('Trying to reconnect'));
  
  
  unset($_SESSION['webmail_plus']['connection_disabled']);

  $mail_api_connection = _webmail_plus_connect();
  
  
  if($mail_api_connection) {
    drupal_set_message(t('Reconnected to the mail server successfully.'));
    drupal_goto(WEBMAIL_PLUS_ALIAS.'/gateway');
  }
    
  $form['error'] = array(
    '#type' => 'item',
    '#value' => t('Could not reconnect. Try !link and logging back in. If the problem persists, please contact the site administrator.', array('!link' => l(t('logging out'), 'logout')))
  );
  
  return $form;
}



?>
