/*
window.onbeforeunload = webmail_plus_leaving_compose();

function webmail_plus_leaving_compose() {
  
    if(document.getElementById('email_to').value!='' ||
    document.getElementById('email_body').value!='' ||
    document.getElementById('email_subject').value!='' ||
    document.getElementById('email_body').value!='') {
      return confirm(Drupal.t('Your message will not be sent. Discard message?'));
    }
}
*/

function webmail_plus_validate_compose() {

  //alert('zazaza');
  //return false;
  
	if(document.getElementById('email_to').value=='') {
	  alert(Drupal.t('Recepient not specified.'));
	  return false;
	}
	
	
	if(document.getElementById('email_body').value=='' && document.getElementById('email_subject').value=='') {
    return confirm(Drupal.t('Send this message without subject and text in the body?'));
	}

  if(document.getElementById('email_subject').value=='') {
    return confirm(Drupal.t('Send this message without a subject?'));
  }

	
  if(document.getElementById('email_body').value=='') {
    return confirm(Drupal.t('Send this message without text in the body?'));
  }

	return true;
}

/**
only pop the question if the message isn't empty, otherwise return true
**/
function webmail_plus_discard_compose() {
  if(document.getElementById('email_to').value!='' ||
     document.getElementById('email_cc').value!='' ||
     document.getElementById('email_bcc').value!='' ||
     document.getElementById('email_subject').value!='' ||
     document.getElementById('email_body').value!='') {
  
      return confirm(Drupal.t('Are you sure you want to discard this message?'));
  }
  else {
    return true;
  }
}