$(document).ready(function() {
  $('.hide-form').hide();
  $('.hideshow-buttons a').click(function() { 
	  var divClass = $(this).attr('href');
	  $('div.'+divClass).toggle();
	  if(divClass == 'message-header') {
			if($(this).text() == 'Show Full Header') {
			  $(this).text('Hide Full Header');
		  } else {
			  $(this).text('Show Full Header');
		  }
	  } else {
		  $(this).hide();
	  }
	  return false;
  });
});