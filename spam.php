<?php
/*
this script sends a ton of messages to the mailbox
*/

$how_many = 10;
$to = 'admin@financeluck.com';
$subject = 'test message %INDEX%';
$message = 'this is the body of the test message %INDEX%';
$headers = 'From: Drupal Tester %INDEX% <admin@financeluck.com>';

for($index=1; $index<=$how_many; $index++) {
	
	$mail_headers = preg_replace('/%INDEX%/', $index, $headers);
	$mail_subject = preg_replace('/%INDEX%/', $index, $subject);
	$mail_message = preg_replace('/%INDEX%/', $index, $message);

	//echo $message;
	
	mail($to,$mail_subject,$mail_message,$mail_headers);
	
	sleep(5);
}
?>