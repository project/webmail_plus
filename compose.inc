<?php

function webmail_plus_compose() {
	global $user, $webmail_plus_config;
	
	//__webmail_plus_dump($webmail_plus_config['delivery_method']);
	
	
	theme('add_style', drupal_get_path('module', 'webmail_plus') . '/webmail_plus.css');

	$action = arg(2);
	if($action=="") $action = "new";

	$folder = arg(3);
	$msgno = arg(4);
	$to = arg(5);
	


	
	// create a mail receipt if we're repying or forwarding
	if(preg_match("/reply|replyall|forward|draft/", $action)) {
		
		global $mail_storage;
		
		$mail_storage -> active_folder(__webmail_plus_resolve_folder_name($folder));
		$header = $mail_storage -> get_header($msgno);


		//__webmail_plus_dump($header);


		// set the fields
		$mail_from = $header->fromaddress;
		
		//die("mailfrom: ".$mail_from);
		
		$mail_to = $header->toaddress;
		$mail_cc = $header->ccaddress;
		$mail_bcc = $header->bccaddress;

		$subject_charset = __webmail_plus_extract_charset($header->subject);
		$mail_subject = decodeHeader($header -> subject, $subject_charset);
		$mail_date = $header->date;



		//$mail_body = preg_replace("/\n/", "\r\n", $mail_storage -> get_plaintext($msgno));
		//$mail_body = preg_replace("/\n/", "\n>", $mail_storage -> get_plaintext($msgno));
		$mail_body = $mail_storage -> get_plaintext($msgno);


		// check if there are attachments
		//$mail_attachments = $mail_storage -> get_attachment_overview($msgno);
		$mail_attachments = $mail_storage -> get_attachment_files($msgno);		
		
		
		
		
		// create a header string used in replies and forwards
		$mail_receipt  = "\r\n\r\n";	// two empty lines so we know wher to start replying
		$mail_receipt .= str_repeat('-',15)." original message ".str_repeat('-',15)."\r\n";
		$mail_receipt .= "Subject: \t".html_entity_decode($mail_subject)."\r\n";
		$mail_receipt .= "From: \t".$mail_from."\r\n";
		$mail_receipt .= "Date: \t".$mail_date."\r\n";
		$mail_receipt .= "To: \t".$mail_to."\r\n";
		$mail_receipt .= str_repeat('-',48)."\r\n";	
		

		
		// based on the action set the correct subject prefix
		switch($action) {
			case "reply":
				$new_subject_prefix="Re:";
				$new_to = $mail_from;
				break;
			case "replyall":
				$new_subject_prefix="Re:";
				$new_to = $mail_from;
				$new_cc = $mail_cc;
				break;
			case "forward":
				$new_subject_prefix="Fwd:";
				$new_subject = $mail_subject;
				break;
		}
		
	}
	

	// if we're forwarding, we want to preserve attachments so we could forward them as well
	if($action=="forward" && sizeof($mail_attachments)>0) {

		$tmp_dir = variable_get('file_directory_temp', '/tmp');

		if(!file_exists($tmp_dir."/".$user->name)) {
			if(!mkdir($tmp_dir."/".$user->name)) {
				watchdog('webmail_plus', 'could not create a subdirectory inside of '.$tmp_dir);
			}
		}


		if($mail_attachments!="") {

			foreach($mail_attachments as $attachment_id => $attachment_info) {

				// only work with files that have names, the rest is various meta data that isn't imporant
				if(!empty($attachment_info['file'])) {
						$_SESSION['attached_files'][$attachment_id] = $attachment_info['file'];
						$file_data = $mail_storage -> get_part($msgno, $attachment_info['part_number']);

					$rs = file_put_contents($tmp_dir."/".$user->name."/".$attachment_info['file'], $file_data);
				}

			}
		}


	}
	
	
	// create a table with attachments
	if(($_SESSION['attached_files'])) {

		$form['attached_files']['#tree']=TRUE;

		foreach($_SESSION['attached_files'] as $attachment_id => $attachment_file) {
			$form['attached_files'][$attachment_id]['checkbox']=array("#type" => "checkbox", "#value"=>"checked");
			$form['attached_files'][$attachment_id]['file']=array('#value'=>$attachment_file);
		}

	}	

		
	$form = array();
	$form['#attributes'] = array('enctype' => 'multipart/form-data');

	$form['op'] = array(
		'#type' => 'hidden'
	);
	
	$form['top_buttons']['#tree']= TRUE;
		
	$form['top_buttons']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Send'),
		
		'#attributes' => array(
			'onclick' => "return webmail_plusComposeOpSend()"
		)
	);

	$form['top_buttons']['save'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
		
		'#attributes' => array(
			'onclick' => "return webmail_plusComposeOpSave()"
		)
	);	
	
	
	$form['top_buttons']['discard'] = array(
		'#type' => 'submit',
		'#value' => t('Discard'),
		
		'#attributes' => array(
			'onclick' => "return webmail_plusComposeOpDiscard()"
		)
	);	

	$form['to'] = array(
		'#type' => 'textfield',
		'#title' => t('To'),
		'#size' => 64,
		'#autocomplete_path' => 'webmail_plus/address_autocomplete'
	);


	if($_SESSION['this_email']['to']) {
		$form['to']['#default_value']=$_SESSION['this_email']['to'];
	} else {
		$form['to']['#default_value']=$new_to;
	}
	
	
	

	$form['cc'] = array(
		'#type' => 'textfield',
		'#title' => t('CC'),
		'#size' => 64,
		'#autocomplete_path' => 'webmail_plus/address_autocomplete'
	);
	
	if($_SESSION['this_email']['cc']) {
		$form['cc']['#default_value']=$_SESSION['this_email']['cc'];
	} else {
		$form['cc']['#default_value']=$new_cc;
	}
	
	//		'#autocomplete_path' => 'webmail_plus/address_autocomplete'
	$form['bcc'] = array(
		'#type' => 'textfield',
		'#title' => t('BCC'),
		'#size' => 64,
		'#autocomplete_path' => 'webmail_plus/address_autocomplete'

	);
	
	if($_SESSION['this_email']['bcc']) {
		$form['bcc']['#default_value']=$_SESSION['this_email']['bcc'];
	} else {
		$form['bcc']['#default_value']=$new_bcc;
	}

	//	'#autocomplete_path' => 'webmail_plus/address_autocomplete'
	
	$form['subject'] = array(
		'#type' => 'textfield',
		'#title' => t('Subject'),
		'#size' => 64
	);

	if($_SESSION['this_email']['subject']) {
		$form['subject']['#default_value']=$_SESSION['this_email']['subject'];
	} else {
		if($new_subject_prefix) {
			$form['subject']['#default_value']=$new_subject_prefix." ".html_entity_decode($mail_subject);
		} else {
			$form['subject']['#default_value']=html_entity_decode($mail_subject);
		}
	}
	

	$form['body'] = array(
		'#type' => 'textarea',
		'#title' => t('Message'),
		'#cols' => 50,
		'#rows' => 10,
	);	
	
	if(preg_match("/reply|replyall|forward/", $action)) {
		$form['body']['#default_value']= $mail_receipt."\r\n".preg_replace("/$|\n|^/", "\n> ", $mail_body);
	} else if (preg_match("/draft/", $action)) {
		$form['body']['#default_value'] = $_SESSION['this_email']['body'];
	} else {
		$form['body']['#default_value'] = $mail_body;
	}
		


	// buttons that are on the bottom
	
	$form['file_buttons']['#tree']= TRUE;
	
	$form['file_buttons']['file'] = array(
		'#type' => 'file',
		'#title' => t('File')
	);
	
	$form['file_buttons']['upload'] = array(
		'#type' => 'submit',
		'#value' => t('Upload'),
		'#attributes' => array(
			'onclick' => "return webmail_plusComposeOpUpload()"
		)	
		
	);

	$form['send2'] = array(
		'#type' => 'submit',
		'#value' => t('Send'),
		
		'#attributes' => array(
			'onclick' => "return webmail_plusComposeOpSend()"
		)		
	);
		
	
	// create a table with attachments
	if(($_SESSION['attached_files'])) {

		$form['attached_files']['#tree']=TRUE;

		foreach($_SESSION['attached_files'] as $attachment_id => $attachment_file) {
			$form['attached_files'][$attachment_id]['checkbox']=array("#type" => "checkbox", "#value"=>"checked");
			$form['attached_files'][$attachment_id]['file']=array('#value'=>$attachment_file);
		}

	}
	
	return drupal_get_form('webmail_plus_compose_form', $form);
}

function theme_webmail_plus_compose_form($form) {

	//__webmail_plus_dump($_SESSION['attached_files']);

/*
	$output = form_render($form);
	return $output;
*/

	$output = "";

	$output .= form_render($form['top_buttons']);
	unset($form['top_buttons']);

	$output .= form_render($form['to']);
	unset($form['to']);

	$output .= form_render($form['cc']);
	unset($form['cc']);

	$output .= form_render($form['bcc']);
	unset($form['bcc']);

	$output .= form_render($form['subject']);
	unset($form['subject']);

	$output .= form_render($form['body']);
	unset($form['body']);


	if($form['attached_files']) {


		//$output .= form_render($form['attached_files']);


		// build rows
		$row=0;
		foreach (element_children($form['attached_files']) as $key) {

			$row++;

			if($row % 2 == 0) {
				$class = 'webmail_plus_row_even';
			} else {
				$class =  'webmail_plus_row_odd';
			}



			// create table rows out of form elements
		    $attachment_rows[] = array(
		    	array('data'=>form_render($form['attached_files'][$key]['checkbox']), 'class'=>$class),
		    	array('data'=>form_render($form['attached_files'][$key]['file']), 'class'=>$class),
	    	);

		}

		// create a form
		$table_attributes = array(
								'width' => '100%'
								);



		$headers = array(
		    			array('data' => t('')),
						array('data' => t('File'))
		    			);

		$output .= theme('table', $headers, $attachment_rows ,$table_attributes );


		unset($form['attached_files']);
	}


/*
	$output .= form_render($form['attachments']);
	unset($form['attachments']);
*/
    $output .= form_render($form['file']);
    unset($form['file']);

    $output .= form_render($form['upload']);
	unset($form['upload']);

	$output .= form_render($form['hidden']);
	unset($form['hidden']);

	$output .= "<br><br>";

	$output .= form_render($form['submit']);
	unset($form['submit']);

	$output .= form_render($form);

	//$output .= form_render($form);

	return $output;
}


// make sure the fields that are getting submitted are valid
function webmail_plus_compose_form_validate($form_id, $form_values) {
	global $mail_storage;
			
	
	// use this only when sending a message
	if($form_values['op']!='send') {
		//echo "nothing to validate";
		return;
	}

	if($form_values['to']) {
		$to_bad = __webmail_plus_bad_email_list($form_values['to']);
		if(sizeof($to_bad)>0) {
			form_set_error('',t('The following To addresses are invalid: %invalid_list', array('%invalid_list' => implode(", ", $to_bad))));
		}
	} else {
		form_set_error('',t('No recepients were supplied.'));
	}
	
	//__webmail_plus_dump(__webmail_plus_map_email_list($form_values['to']));
	
	
	if($form_values['cc']) {
		$cc_bad = __webmail_plus_bad_email_list($form_values['cc']);
		if(sizeof($cc_bad)>0) {
			form_set_error('',t('The following CC addresses are invalid: %invalid_list', array('%invalid_list' => implode(", ", $cc_bad))));
		}
	}
	
	
	if($form_values['bcc']) {
		$bcc_bad = __webmail_plus_bad_email_list($form_values['bcc']);
		if(sizeof($bcc_bad)>0) {
			form_set_error('',t('The following BCC addresses are invalid: %invalid_list', array('%invalid_list' => implode(", ", $bcc_bad))));
		}
	}	
	

}

function webmail_plus_compose_form_submit($form_id, $form_values) {
	
	global $user, $webmail_plus_config, $mail_storage;
	
	
	// the user clicked discard button
	if($form_values['op']=='discard') {
		return 'webmail_plus/inbox/'.$_SESSION['webmail_plus_active_folder'];
	}
	
	
	if($form_values['op']=='save') {
		

		
		// upload the file to the email being sent if the file is not empty
        if(!empty($_FILES['edit']['name']['file_buttons']['file'])) {
        	$tmp_dir = variable_get('file_directory_temp', '/tmp');
            // if the directory does not exist already create it
            if(!file_exists($tmp_dir."/".$user->name)) {
            	if(!mkdir($tmp_dir."/".$user->name)) {
	            	watchdog('webmail_plus', 'could not create a subdirectory inside of '.$tmp_dir);
                }
			}

            // move the uploaded file to users subdirectory
            if(move_uploaded_file($_FILES['edit']['tmp_name']['file_buttons']['file'], $tmp_dir."/".$user->name."/".$_FILES['edit']['name']['file_buttons']['file'])) {
            	$_SESSION['attached_files'][]=$_FILES['edit']['name']['file'];
			} else {
				//die('could not move file');
				watchdog('webmail_plus','could not place a file into '.$tmp_dir."/".$user->name, WATCHDOG_ERROR);
			}

            // need to add to the form values attached files so this file will attached also
            $len = sizeof($form_values['attached_files']);
            $form_values['attached_files'][$len] = $_FILES['edit']['name']['file_buttons']['file'];

        }
        
        
		// i don't have an explantation why i am forced to use $_POST variable instead of $form_values
		$message_body = $form_values['body'];




		$new_headers = "";
		if($form_values['to']) $new_headers .= "To: ".$form_values['to']."\n";
		if($form_values['cc']) $new_headers .= "Cc: ".$form_values['cc']."\n";
		if($form_values['bcc']) $new_headers .= "Bcc: ".$form_values['bcc']."\n";
		if($form_values['subject']) $new_headers .= "Subject: ".$form_values['subject']."\n";
		$new_headers .= "Date: ".date("r")."\n";


		$message_body = $form_values['body'];


		// write the new one
		$msgno=arg(4);
		//echo "msgno: $msgno<br>\n";

		if($msgno) {
			$mail_storage -> active_folder(__webmail_plus_resolve_folder_name("Drafts"));
			$rs1 = $mail_storage -> delete($msgno);
			$mail_storage -> expunge();
			//die("deleting message ".$msgno);
		}

		$rs2 = $mail_storage -> append(__webmail_plus_resolve_folder_name("Drafts"), $new_headers."\r\n\r\n".$message_body);

		__webmail_plus_clear_folder_cache('Drafts');

		// mark the message seen
		$mail_storage -> active_folder(__webmail_plus_resolve_folder_name('Drafts'));
		$overview = $mail_storage -> get_overview();
		$messages = sizeof($overview);
		$mail_storage -> set_flag($messages, '\\Seen');

		$mail_storage -> expunge();



	    return 'webmail_plus/compose/draft/Drafts/'.$messages;
        
        
	}
	
	// handle file uplaods
	if($form_values['op']=='upload') {
		
		/*
		in some crazy way $form_values get reset and it makes it impossible to use them here to
		store the session, thus this hack. this will  probably get resolved in drupal 5 since
		FormAPI there are smoother.
		*/


		if(!empty($form_values['to'])) $_SESSION['this_email']['to']=$form_values['to'];
		if(!empty($form_values['cc'])) $_SESSION['this_email']['cc']=$form_values['cc'];
		if(!empty($form_values['bcc'])) $_SESSION['this_email']['bcc']=$form_values['bcc'];
		if(!empty($form_values['subject'])) $_SESSION['this_email']['subject']=$form_values['subject'];
		if(!empty($form_values['body'])) $_SESSION['this_email']['body']=$form_values['body'];		
		
		
		// create a temporary file directory 
		$tmp_dir = variable_get('file_directory_temp', '/tmp');	
		
		//echo "tmp_dir: ".$tmp_dir;
			
		if(!file_exists($tmp_dir."/".$user->name)) {
			if(!mkdir($tmp_dir."/".$user->name, 0777)) {
				//die('could not create a subdirectory inside of '.$tmp_dir);
				watchdog('webmail_plus', 'could not create a subdirectory inside of '.$tmp_dir);
			}
		}	
		

		//__webmail_plus_dump($_FILES);
		
		//echo "moving to ".$_FILES['edit']['tmp_name']['file_buttons']['file']." to ".$_FILES['edit']['name']['file_buttons']['file']."<br>";
		
		//__webmail_plus_dump($_FILES);
		
		
		// move the uploaded file to users subdirectory
		//echo "copying ".$_FILES['edit']['tmp_name']['file_buttons']['file']." to ".$tmp_dir."/".$user->name."/".$_FILES['edit']['name']['file_buttons']['file']."<br>\n";
		
		if(move_uploaded_file($_FILES['edit']['tmp_name']['file_buttons']['file'], $tmp_dir."/".$user->name."/".$_FILES['edit']['name']['file_buttons']['file'])) {
			$_SESSION['attached_files'][]=$_FILES['edit']['name']['file_buttons']['file'];
		} else {
			//die('could not place a file into '.$tmp_dir."/".$user->name);
			watchdog('webmail_plus','could not place a file into '.$tmp_dir."/".$user->name, WATCHDOG_ERROR);
		}		
		
		// this actually needs to be modified for reply and forward cases
		return 'webmail_plus/compose';	
	}
	
	
	
	// the user clicked send button
	if($form_values['op']=='send') {
		
        // upload the file to the email being sent if the file is not empty
        if(!empty($_FILES['edit']['name']['file_buttons']['file'])) {
        	$tmp_dir = variable_get('file_directory_temp', '/tmp');
            // if the directory does not exist already create it
            if(!file_exists($tmp_dir."/".$user->name)) {
            	if(!mkdir($tmp_dir."/".$user->name)) {
	            	watchdog('webmail_plus', 'could not create a subdirectory inside of '.$tmp_dir);
                }
			}

            // move the uploaded file to users subdirectory
            if(move_uploaded_file($_FILES['edit']['tmp_name']['file_buttons']['file'], $tmp_dir."/".$user->name."/".$_FILES['edit']['name']['file_buttons']['file'])) {
            	$_SESSION['attached_files'][]=$_FILES['edit']['name']['file_buttons']['file'];
			} else {
				//die('could not move file');
				watchdog('webmail_plus','could not place a file into '.$tmp_dir."/".$user->name, WATCHDOG_ERROR);
			}

            // need to add to the form values attached files so this file will attached also
            $len = sizeof($form_values['attached_files']);
            $form_values['attached_files'][$len] = $_FILES['edit']['name']['file'];

        }
        	
		// actual sending
		$mail = new PHPMailer2();

		if($webmail_plus_config['delivery_method'] == "SMTP") {
			
			// SMTP authentiation isn't yet supported
			watchdog("webmail_plus", "sending via SMTP ".$webmail_plus_config['smtp_host']);
			$mail->IsSMTP();
			$mail->Host = $webmail_plus_config['smtp_host'];  // specify main and backup server
			$mail->SMTPAuth = FALSE;     // turn on SMTP authentication
			
		} else {
			
			watchdog("webmail_plus", "sending via localhost");
			$mail->IsMail();
			
		}		
		
		
		$mail->IsHTML(false);
		
		
		// set TO addresses
		if(!empty($form_values['to'])) {
			$to_map = __webmail_plus_map_email_list($form_values['to']);
			
			foreach($to_map as $simple_email => $complete_email) {
				$mail->AddAddress($complete_email); 
			}
			
		}
		
		// set CC addresses
		if(!empty($form_values['cc'])) {
			$cc_map = __webmail_plus_map_email_list($form_values['to']);
			
			foreach($cc_map as $simple_email => $complete_email) {
				$mail->AddCC($complete_email); 
			}
			
		}
		

		// set BCC addresses
		if(!empty($form_values['bcc'])) {
			$bcc_map = __webmail_plus_map_email_list($form_values['to']);
			
			foreach($bcc_map as $simple_email => $complete_email) {
				$mail->AddBCC($complete_email); 
			}
			
		}



		
		
	    	$mail->From = $user -> name.'@'.$webmail_plus_config['domain'];
	  	//$mail->Sender = "postmaster@".$webmail_plus_config['domain'];
	  	
	  	//echo "sender: postmaster@".$webmail_plus_config['domain'];
	  	
	  	$mail -> CharSet = "UTF-8";

		$mail->Subject = encodeHeader($form_values['subject'], "UTF-8");
		$mail->Body    = __webmail_plus_encode($form_values['body'], "UTF-8");
		//$mail->AltBody = $_POST['edit']['body'];

		
		// handle attachments, if any
		if(sizeof($form_values['attached_files'])>0) {
			$tmp_dir = variable_get('file_directory_temp', '/tmp');
			foreach($form_values['attached_files'] as $attached_file_id=>$attached_file_info) {
				//echo "Attaching ".$_SESSION['attached_files'][$attached_file_id]."<br>\n";
				if(file_exists($tmp_dir."/".$user->name."/".$_SESSION['attached_files'][$attached_file_id])) {
					$mail->AddAttachment($tmp_dir."/".$user->name."/".$_SESSION['attached_files'][$attached_file_id]);
				}
			}
		}
				
		
		$mail->Send();
		
		
		
		// this adds the message to the sent folder
		$message_to = $form_values['to'];
		$message_from = $form_values['from'];
		$message_subject = $mail -> EncodeHeader($form_values['subject']);
		$message_header = $mail -> header;
		$message_header = "To: ".$message_to."\r\nSubject: ".$message_subject."\r\n".$message_header;
		$message_body = $form_values['body']."\n";
		
		$mail_storage -> append(__webmail_plus_expand_folder("Sent"), $message_header."\r\n\r\n".$mail -> body);
		
		// mark the message seen
		$mail_storage -> active_folder(__webmail_plus_expand_folder('Sent'));
		$overview = $mail_storage -> get_overview();
		$messages = sizeof($overview);
		$mail_storage -> set_flag($messages, '\\Seen');
		
		// now clean up the files
		if(sizeof($form_values['attached_files'])>0) {
			foreach($form_values['attached_files'] as $attached_file_id=>$attached_file_info) {
			//echo "unsetting attached files...<br>\n";
			unset($_SESSION['attached_files'][$attached_file_id]); // remove it from session
			@unlink($tmp_dir."/".$user->name."/".$_SESSION['attached_files'][$attached_file_id]);
			}
		}
		__webmail_plus_clear_folder_cache('Sent');		
		
		// set the subject so we can show the notificaiton
		$_SESSION['webmail_plus_message_sent']=__webmail_plus_format_subject($form_values['subject']);
                		
		// redirect the user back where he came from
		return 'webmail_plus/inbox/'.$_SESSION['webmail_plus_active_folder'];
	}
	


	
}


function webmail_plus_address_autocomplete($string='') {

	

	global $user, $ldap, $webmail_plus_config;


	// extract the last item
	$pieces = preg_split("/(\s|,)/", $string);
	$last = $pieces[sizeof($pieces)-1];


	if(strlen($last)<3) return;



	$items = split(',', $string);
	$search = trim($items[count($items) - 1]);
	unset($items[count($items) - 1]);
	$base_string = implode(',' ,$items);

	if($search == '') {
    	return;
	}

	if(trim($base_string) != '') {
		$base_string .= ', ';
	}


	$matches = array();


	// search the private address book
	if($search) {
		 // CONCAT(first_name,' ',last_name)

		$result = db_query_range("SELECT nickname,first_name,last_name,email FROM webmail_plus_address_book WHERE uid=%d AND nickname LIKE '%s%%' OR first_name LIKE '%s%%' OR last_name LIKE '%s%%'", $user->uid, $search, $search, $search, 0,10);


		while ($record = db_fetch_object($result)) {
	    	//$matches[$base_string.$nickname->nickname] = check_plain($nickname->nickname);
	    	$full_email = "\"$record->first_name $record->last_name\" <".__webmail_plus_format_address($record->email).">";
	    	$matches[$base_string.$full_email] = __webmail_plus_format_address($full_email);
		}


	}


	//print drupal_to_js($matches);
	//exit();



	// search LDAP, if avaiable
	if($search && module_exist('ldapdata') && is_object($ldap)) {



		$dn = $webmail_plus_config['ldap_user_attribute'].'='.$user -> name.','.$webmail_plus_config['ldap_base_dn'];

		//echo $dn;

		$ldap -> connect($dn, $webmail_plus_config['user_password']);

		$rs = $ldap -> search_list($webmail_plus_config['ldap_base_dn'], "(CN=*$search*)", array('cn', 'mail'));
			$ldap -> disconnect();

			foreach($rs as $index=>$record) {
				if(!empty($record['mail'][0])) {
					$matches[$base_string.$record['mail'][0]]=$record['mail'][0];
				}

			}


	}

	// make sure nothing gets cached
	header("Cache-Control: no-cache, must-revalidate");
  	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");

	print drupal_to_js($matches);

	exit();
}
