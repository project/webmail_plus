<?php
function webmail_plus_message_view_form() {
	global $user, $webmail_plus_config, $mail_storage;


	
	//$_SESSION['webmail_plus_waiting_messages']['unseen']=0;

	if(!$_SESSION['webmail_plus_mail_storage_connected']) {
		drupal_goto('webmail_plus/misconfigured');
	}

	$folder = arg(2);


	$msgid = __webmail_plus_extract_msgid(arg(3));




	// see if this message was seen or not
	$seen = db_result(db_query("SELECT seen FROM {webmail_plus_user_headers} WHERE id=%d AND uid=%d AND folder='%s' LIMIT 1", $msgid, $user->uid, $folder));

	// update the message as seen for the folder bar
	if($seen!=1) {
		$_SESSION['webmail_plus_block_unseen'][$folder]--;
		$_SESSION['webmail_plus_waiting_messages']['unseen']--;


		// denis here

		//$_SESSION['webmail_plus_search_results'][$felder]
		db_query("UPDATE {webmail_plus_user_headers} SET seen=1 WHERE id=%d AND uid=%d AND folder='%s'",$msgid, $user->uid, $folder);
	}


	
	//print_r(node_get_types());

	$form = array();



	$form['op'] = array(
		'#type' => 'hidden'
	);	


//	$form['buttons']['#prefix'] = '<div class=webmail_plus_inline>';
//	$form['buttons']['#suffix'] = '</div>';

	$form['buttons']['back'] = array(
		'#type' => 'submit',
		'#value' => 'Back',
		'#attributes' => array('onclick' => 'history.back()'),
		'#attributes' => array(
			'onclick' => "return webmail_plusMessageOpBack()"
		)
	);


	$form['buttons']['reply'] = array(
		'#type' => 'submit',
		'#value' => 'Reply',
		'#attributes' => array(
			'onclick' => "return webmail_plusMessageOpReply()"
		)
	);

	$form['buttons']['reply_all'] = array(
		'#type' => 'submit',
		'#value' => 'Reply All',
		'#attributes' => array(
			'onclick' => "return webmail_plusMessageOpReplyAll()"
		)
	);

	$form['buttons']['forward'] = array(
		'#type' => 'submit',
		'#value' => 'Forward',
		'#attributes' => array(
			'onclick' => "return webmail_plusMessageOpForward()"
		)
	);

	$form['buttons']['delete'] = array(
		'#type' => 'submit',
		'#value' => 'Delete',
		'#attributes' => array(
			'onclick' => "return webmail_plusMessageOpDelete()"
		)
	);

	
	$form['buttons']['print'] = array(
		'#type' => 'submit',
		'#value' => 'Print',
		'#attributes' => array(
			'onclick' => "window.open('?q=webmail_plus/message_print/".$folder."/".$msgid."', '_blank', 'scrollbars=yes,status=no,titlebar=no,location=no'); return false;"
		)

	);



	$form['buttons']['previous'] = array(
		'#type' => 'submit',
		'#value' => 'Previous',
		'#attributes' => array(
			'onclick' => "return webmail_plusMessageOpPrevious()"
		)
	);


	$form['buttons']['next'] = array(
		'#type' => 'submit',
		'#value' => 'Next',
		'#attributes' => array(
			'onclick' => "return webmail_plusMessageOpNext()"
		)
	);





	$form['buttons']['move'] = array(
		'#type' => 'submit',
		'#value' => 'Move To:',
		'#attributes' => array(
			'onclick' => "return webmail_plusMessageOpMoveTo()"
		)
	);

	// create a list of folders
	$form['buttons']['moveto_folder']= array(
		"#type" => "select",
		"#options" => drupal_map_assoc(__webmail_plus_get_folders()),
		'#default_value' => variable_get('moveto_folder',$folder)
	);


	// only show this if the administrator allowed the publish feature in settings
	if($webmail_plus_config['can_publish_nodes'] && sizeof(__webmail_plus_get_publish_nodes())>0 ) {
		
		// this is publish as
		$form['buttons']['publish_as'] = array(
			'#type' => 'submit',
			'#value' => 'Publish As: ',
			'#attributes' => array(
				'onclick' => "return webmail_plusMessageOpPublish()"
			)
		);
	
	
		// create a list of folders
		$form['buttons']['node_type']= array(
			"#type" => "select",
			"#options" => drupal_map_assoc(__webmail_plus_get_publish_nodes())
		);

	}
	// fetch the header

	// have to do this to avoid errors]

	//echo "webmail_plus active folder: ".$_SESSION['webmail_plus_active_folder']."<br>\n";



	$mail_storage -> active_folder(__webmail_plus_resolve_folder_name($folder));
	$overview = $mail_storage -> get_overview();

	
	// check if the body is cached
	/*
	if($webmail_plus_config['cache_body']) {

		$body = db_result(db_query("SELECT body FROM {webmail_plus_user_body} WHERE id=%d AND uid=%d AND folder='%s'", $msgid, $user->uid, $folder));
	}
	*/
	
	//__webmail_plus_dump($overview[$msgid]);

	if($overview[$msgid]) {

		// fetch the map, we'll need it to get the charset
		$map = $mail_storage -> get_map($msgid);
		$body_charset =  $mail_storage -> get_plaintext_charset($msgid);

		if($body_charset=="") {
			//echo "charset is empty";
			$body_charset="iso-8859-1";
		}


		$body_encoding = $mail_storage -> get_plaintext_encoding($msgid);

		$mail_storage -> active_folder(__webmail_plus_resolve_folder_name($folder));

		$header = $mail_storage -> get_header($msgid);

		$subject_charset = __webmail_plus_extract_charset($header -> subject);

		$subject = __webmail_plus_decode(decodeHeader($header -> subject), $subject_charset);

		
		$form['header']['from'] = array('#value' => __webmail_plus_format_address($header->fromaddress), '#class' => 'webmail_plus_email_header');
		$form['header']['to'] = array('#value' => __webmail_plus_format_address($header->toaddress), '#class' => 'webmail_plus_email_header');
		$form['header']['cc'] = array('#value' => __webmail_plus_format_address($header->ccaddress), '#class' => 'webmail_plus_email_header');
		$form['header']['date'] = array('#value' => $header->date, '#class' => 'webmail_plus_email_header');
		$form['header']['subject'] = array('#value' => $subject, '#type' => 'hidden', '#class' => 'webmail_plus_email_header');

		// see if the address is in the address book
		if(!__webmail_plus_address_in_book($form['header']['from']['#value'])) {

			$form['header']['address_in_book'] = array('#value' => 'yes', '#type' => 'hidden');

		} else {

			$form['header']['address_in_book'] = array('#value' => 'no', '#type' => 'hidden');

		}
		
		

		
		
		
		// see if a local user or not
		if(__webmail_plus_valid_user(__webmail_plus_extract_email($form['header']['from']['#value']))) {
			$form['header']['is_local'] = array('#value' => 'yes', '#type' => 'hidden');
		} else {
			$form['header']['is_local'] = array('#value' => 'no', '#type' => 'hidden');
		}


		
		// fetch the body
		$request_options=arg(4);

		// see if Do-Not-Touch flag is set
		if($request_options='dnt') {

			$options = "FT_PEEK";
		}

		if($charset!="iso-8859-1") {
			$body = __webmail_plus_decode($mail_storage -> get_plaintext($msgid, $options), $body_charset);
		} else {
			$body = htmlspecialchars(__webmail_plus_decode($mail_storage -> get_plaintext(arg(3), $options), $body_charset));

		}

		$body = __webmail_plus_make_clickable($body);


		$form['body']['default'] = array('#value' => preg_replace("/\n/", "<br>", $body));

		// fetch attachments, if there are any
		$attachments = $mail_storage ->  get_attachment_overview($msgid);


		db_query("UPDATE {webmail_plus_user_headers} SET seen=1 WHERE uid=%d AND folder='%s' AND id=%d", $user->uid, $folder, arg(3));


	
	} else {
		// once we get to the last message, just show the contents of the folder
		//die('redirecting');
		drupal_goto("webmail_plus/inbox/".$folder);
	}



	// if body caching is enabled, update the cache
	/*
	if($webmail_plus_config['cache_body']) {

		$rs = db_query("INSERT IGNORE INTO {webmail_plus_user_body}(id,uid,folder,body,charset) VALUES(%d,%d,'%s','%s','%s')", $msgid, $user->uid, $folder, $body, $body_charset);
	}
	*/



	if(!empty($attachments)) {
		//$form['attachments']['#tree'] = TRUE;


		// put this into forms
		foreach($attachments as $attachment_id=>$attachment_info) {


			if(!empty($attachment_info['file'])) {
				$form['attachments'][$attachment_id]['part_number']=array('#value'=>$attachment_info['part_number'], '#type'=>'hidden');
				$form['attachments'][$attachment_id]['type']=array('#value'=>$attachment_info['type'], '#type' => 'hidden');
				$form['attachments'][$attachment_id]['size']=array('#value'=>__webmail_plus_reformat_size($attachment_info['size']));
				$form['attachments'][$attachment_id]['file']=array('#value'=>$attachment_info['file'], '#type' => 'hidden');
			}
		}

	}


	$form['extra']['folder'] = array('#value' => arg(2), '#type' => 'hidden');
	$form['extra']['msgno'] = array('#value' => arg(3), '#type' => 'hidden');

	//print_r($form);

	return drupal_get_form('webmail_plus_message_view_form', $form);

}



function theme_webmail_plus_message_view_form($form) {

	//print_r($form);

	if(empty($form['header']['from'])) {
		drupal_set_title('Message does not exist.');

		$content.='This message does not exist. You probably reached the last message.';
		return $content;
	}


/*	echo "<pre>\n";
	echo "subject: ".$form['header']['subject']['#value'];
	echo "</pre>\n";*/

	drupal_set_title($form['header']['subject']['#value']." (".date("M d, Y h:i:s A", strtotime($form['header']['date']['#value'])).")");

	$output = "<br>\n";

	$output .= preg_replace("/<p>/i", "", form_render($form['buttons']));

	unset($form['buttons']);

	$output .= "<br><br>\n";

	$rows[] = array(
  		'To: ',
  		$form['header']['to']['#value']
  	);



  	if($form['header']['is_local']['#value']=='yes' && module_exist('pathauto')) {

  		$rows[] = array(
	  		'From: ',
	  		$form['header']['from']['#value'].' '.l('View Profile', 'user/'.__webmail_plus_lookup_user_id(__webmail_plus_extract_user(__webmail_plus_extract_email($form['header']['from']['#value']))))
	  	);

  	} else {

	  	$rows[] = array(
	  		'From: ',
	  		$form['header']['from']['#value']
	  	);
  	}


  	// no need to show empty CC
  	if(!empty($form['header']['cc']['#value'])) {
	  	$rows[] = array(
	  		'Cc: ',
	  		$form['header']['cc']['#value']
	  	);
  	}



  	/*
  	$rows[] = array(
  		'Subject: ',
  		$form['header']['subject']['#value']
  	);
  	*/

  	$output .= theme('table', $headers, $rows);

  	unset($form['header']);

	$output .= "<br><br>\n";

	$output .= "<div class='webmail_plus_message_body'>";
	$output .= form_render($form['body']['default']);
	$output .= "</div>";
	$output .= "<br><br>";
	unset($form['body']);



	/* handle attachments */

	$row=0;
	foreach (element_children($form['attachments']) as $key) {

		$row++;

		if($row % 2 == 0) {
			$class = 'webmail_plus_row_even';
		} else {
			$class =  'webmail_plus_row_odd';
		}



		// create table rows out of form elements
	    $attachment_rows[] = array(
	    	array('data'=>form_render($form['attachments'][$key]['part_number']),'class'=>$class),
			array('data'=>l(__webmail_plus_format_filename($form['attachments'][$key]['file']['#value']), 'webmail_plus/download_attachment/'.$_SESSION['webmail_plus_active_folder'].'/'.arg(3).'/'.$form['attachments'][$key]['part_number']['#value']),'class'=>$class),
	    	array('data'=>form_render($form['attachments'][$key]['size']),'class'=>$class),
	    	array('data'=>form_render($form['attachments'][$key]['type']),'class'=>$class)
		);

	}


	if(sizeof($form['attachments'])>0) {

		$output .= "<br>\n";

		$table_attributes = array(
								'width' => '100%'
								);



		$headers = array(
						array('data' => t('')),
		    			array('data' => t('File')),
		    			array('data' => t('Size')),
		    			array('data' => t('Type'))
		    			);

		$output .= theme('table', $headers, $attachment_rows ,$table_attributes );

		$output .= form_render($form['attachments']);

	}



	$output .= form_render($form['extra']);
	unset($form['extra']);
	
	$output .= form_render($form);
	return $output;
}




function webmail_plus_message_view_form_submit($form_id,$form_values) {
	global $user, $webmail_plus_config, $mail_storage;



	$folder = $form_values['folder'];
	$msgno = $form_values['msgno'];

	if(preg_match("/publish/i", $form_values['op']))
	{

		// need to do a reverse lookup
		$available_nodes = array_flip(node_get_types());
		$node_key = $available_nodes[$form_values['node_type']];


		return 'node/add/'.$node_key.'/webmail_plus/'.$folder.'/'.$msgno;
	}

	if($form_values['op'] == "back")
	{
		drupal_goto('webmail_plus/inbox/'.$_SESSOIN['webmail_plus_active_folder']);
	}


	if($form_values['op'] == "reply")
	{
		drupal_goto('webmail_plus/compose/reply/'.$folder.'/'.$msgno);
	}

	if($form_values['op'] == "replyall")
	{
		drupal_goto('webmail_plus/compose/replyall/'.$folder.'/'.$msgno);
	}

	if($form_values['op'] == "forward")
	{
		drupal_goto('webmail_plus/compose/forward/'.$folder.'/'.$msgno);
	}

	if($form_values['op'] == "delete") {

		//echo "deleting $msgno<br>";

		$folder = $form_values['folder'];
		$msgno = $form_values['msgno'];


		//echo $folder;



		$mail_storage -> move($msgno, __webmail_plus_resolve_folder_name('Trash'));
		$mail_storage -> expunge();

		db_query("DELETE FROM webmail_plus_user_headers WHERE uid=%d AND folder='%s'", $user->uid, $folder);

		unset($_SESSION['webmail_plus_last_imap_query'][$folder]);
		unset($_SESSION['webmail_plus_last_imap_query']['Trash']);
		return 'webmail_plus/inbox/'.$folder;

	}

	if($form_values['op'] == 'print') {
		$folder = $form_values['folder'];
		$msgno = $form_values['msgno'];
		
		return('webmail_plus/message_print/'.$folder.'/'.$msgno);
	
	}

	if($form_values['op'] == "moveto") {

		
		$mail_storage -> move($form_values['msgno'], __webmail_plus_resolve_folder_name($form_values['moveto_folder']));
		$mail_storage -> expunge();

		unset($_SESSION['webmail_plus_last_imap_query'][$folder]);
		unset($_SESSION['webmail_plus_last_imap_query'][$form_values['moveto_folder']]);

		return 'webmail_plus/inbox/'.$_SESSION['webmail_plus_active_folder'];
	}


	// makes a message as read
	if($form_values['op'] == 'previous') {
		//print_r($form_values);

		//$rs = $mail_storage -> set_flag($form_values['msgno'], "\\Seen");


		$prev_msgno = $form_values['msgno']-1;

		return "webmail_plus/message_view/".$folder."/".$prev_msgno;
		//print_r($rs);
	}

	// marks a message as unread
	if($form_values['op'] == 'next') {

		$next_msgno = $form_values['msgno']+1;

		return "webmail_plus/message_view/".$folder."/".$next_msgno;
	}

}

function webmail_plus_download_attachment_form() {
	global $user, $webmail_plus_config, $mail_storage;


	$folder = arg(2);
	$mid = arg(3);
	$part = arg(4);

	$mail_storage -> active_folder(__webmail_plus_resolve_folder_name($_SESSION['webmail_plus_active_folder']));


	$map = $mail_storage -> get_map($mid);


	$part_info = $mail_storage -> get_part_object($mid, $part);

	if(empty($part_info)) {

		drupal_set_title("This attachment doesn't exist");
		$content .= "The attachmnet you're trying to download does not exist.";
		return $content;
		exit();
	}


	$part = $mail_storage -> get_part($mid,$part);



	// write the headers
	header("Pragma: public");
    header("Expires: 0");
    header('Content-Type: ' . $mail_storage -> get_part_mime_type($mid, $part));
    header('Content-Length: ' . $part_info -> bytes);
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false);
    //header("Content-Disposition: inline");
    header('Content-Disposition: attachment; filename="'.$part_info -> parameters[0]->value.'"');
	echo $part;

	//die("getting $folder/$mid/$part");
}


/*
shows a clean version of the message, without the surrounding drupal layout
*/
function webmail_plus_message_print_form() {
	global $user, $webmail_plus_config, $mail_storage;

	
	//$_SESSION['webmail_plus_waiting_messages']['unseen']=0;

	if(!$_SESSION['webmail_plus_mail_storage_connected']) {
		drupal_goto('webmail_plus/misconfigured');
	}

	$folder = arg(2);


	$msgid = __webmail_plus_extract_msgid(arg(3));
	
	$mail_storage -> active_folder(__webmail_plus_resolve_folder_name($folder));
	$overview = $mail_storage -> get_overview();

	//__webmail_plus_dump($overview[$msgid]);

	if($overview[$msgid]) {

		// fetch the map, we'll need it to get the charset
		$map = $mail_storage -> get_map($msgid);

/*		echo "<pre>";
		print_r($map);
		echo "</pre>";*/

		$body_charset =  $mail_storage -> get_plaintext_charset($msgid);
		//echo "body charset is $body_charset<br>\n";

		if($body_charset=="") {
			//echo "charset is empty";
			$body_charset="iso-8859-1";
		}


		$body_encoding = $mail_storage -> get_plaintext_encoding($msgid);






		$mail_storage -> active_folder(__webmail_plus_resolve_folder_name($folder));

		$header = $mail_storage -> get_header($msgid);


		//echo $header -> subject."<br>";

		$subject_charset = __webmail_plus_extract_charset($header -> subject);

		//echo "subject charset: $subject_charset<br>";

		$subject = __webmail_plus_decode(decodeHeader($header -> subject), $subject_charset);

		//echo decodeHeader($subject);


		$form['header']['from'] = array('#value' => __webmail_plus_format_address($header->fromaddress), '#class' => 'webmail_plus_email_header');
		$form['header']['to'] = array('#value' => __webmail_plus_format_address($header->toaddress), '#class' => 'webmail_plus_email_header');
		$form['header']['cc'] = array('#value' => __webmail_plus_format_address($header->ccaddress), '#class' => 'webmail_plus_email_header');
		$form['header']['date'] = array('#value' => $header->date, '#class' => 'webmail_plus_email_header');
		$form['header']['subject'] = array('#value' => $subject, '#type' => 'hidden', '#class' => 'webmail_plus_email_header');

		// see if the address is in the address book
		if(!__webmail_plus_address_in_book($form['header']['from']['#value'])) {

			$form['header']['address_in_book'] = array('#value' => 'yes', '#type' => 'hidden');

		} else {

			$form['header']['address_in_book'] = array('#value' => 'no', '#type' => 'hidden');

		}


		// see if a local user or not
		if(__webmail_plus_valid_user(__webmail_plus_extract_email($form['header']['from']['#value']))) {
			$form['header']['is_local'] = array('#value' => 'yes', '#type' => 'hidden');
		} else {
			$form['header']['is_local'] = array('#value' => 'no', '#type' => 'hidden');
		}


		// fetch the body
		$request_options=arg(4);

		// see if Do-Not-Touch flag is set
		if($request_options='dnt') {

			$options = "FT_PEEK";
		}





		//$body = charset_decode_koi8_r($mail_storage -> get_plaintext(arg(3), $options));

		//echo "charset: $charset<br>";

		if($charset!="iso-8859-1") {
			$body = __webmail_plus_decode($mail_storage -> get_plaintext($msgid, $options), $body_charset);
		} else {
			$body = htmlspecialchars(__webmail_plus_decode($mail_storage -> get_plaintext(arg(3), $options), $body_charset));

		}

		$body = __webmail_plus_make_clickable($body);

		//$body = $mail_storage -> get_plaintext(arg(3), $options);


		//echo __webmail_plus_decode(base64_decode($body), $charset)."<br>";


		$form['body']['default'] = array('#value' => preg_replace("/\n/", "<br>", $body));
	}	
	
	?>

<html>
<head>
<script>
function Print() {
document.body.offsetHeight;
window.print();
}
</script>
</head>

<body onload="Print()">
<?
	
	
	echo "<h1>".$form['header']['subject']['#value']."</h1>";
	
	echo "<hr>";
	echo "From: ".$form['header']['from']['#value']."<br>\n";
	echo "Date: ".$form['header']['date']['#value']."<br>\n";
	echo "<hr>";
	
	echo $form['body']['default']['#value'];
?>
</body>
</html>
<?
}

/*
this function allows a user to turn any email into any node currently avaible on drupal
it relies on the form_alter hook to pre-populate fields with email info
*/
function webmail_plus_form_alter($form_id, &$form)
{
	global $user, $webmail_plus_config, $mail_storage;




	if(arg(3)!='webmail_plus' && arg(1)!="node")
	{
		return;
	}


	$folder = arg(4);


	$msgno = __webmail_plus_extract_msgid(arg(5));




	if(empty($folder) || empty($msgno))
	{
		return;

	}


	// get the header
	$mail_storage -> active_folder(__webmail_plus_resolve_folder_name($folder));
	$header = $mail_storage -> get_header($msgno);


	// fetch the body
	$body = $mail_storage -> get_plaintext($msgno);


	//echo arg(4)."-".arg(5)."<br>\n";


/*	echo "<pre>";
	print_r($form);
	echo "</pre>";*/

	if($form_id=='story_node_form' || $form_id=='page_node_form' || $form_id=='blog_node_form') {

		$form['title']['#value']=__webmail_plus_format_subject($header->subject);
		$form['body_filter']['body']['#default_value']=$body;
	}
}

?>