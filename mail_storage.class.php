<?php

// this class has this abstract name so that it can be replaced with any other storage mechanism


class mail_storage {
	
	var $mbox = NULL;
	var $host;
	var $port = 143;
	var $open = FALSE;
	var $username;
	var $password;
	var $options = '/notls';
	var $default_folder = 'INBOX';
	var $folder;
	var $connect_string;
	var $msgno;
	var $results;
	var $errors;
	
	
	function __is_connected() {
		if(!$this->mbox) { 
			return FALSE;
			$this->errors[]="no_available_connection";	
		}
		
	}
	
	// open the imap connection
	function connect() {
		$this -> connect_string = '{'.$this->host.':'.$this->port.$this->options.'}'.$this->default_folder;
		
		if(!empty($this->folder) && $this->folder != $this->default_folder) {
			$full_connect_string = $this->connect_string.'.'.$this->folder;
			
		} else {
			$full_connect_string = $this->connect_string;	
		}
		
		//echo $full_connect_string;
		
		if($this -> mbox = imap_open($full_connect_string, $this->username, $this->password)) {
			$this->results = TRUE;
		} else {
			$this->results = FALSE;
		}
		
		return $this->results;
		//echo $connect_string;
	}

	
	function headers() {
		if($this->mbox) {
			$this->results = imap_headers($this->mbox);
			
			// need to reformat them
			foreach(imap_headers($this->mbox) as $index=>$line) {
				preg_match("/\s([0-9]{1,10})\)/", $line, $matches);
				$msgno = $matches[1];
				
				//echo "fetching message $msgno<br>";
				
				$message_header = imap_headerinfo($this->mbox, $msgno);

				$headers[]=$message_header;
				
			}
			
			$this->results = $headers;
			return $headers;
			
		} else {
			$this->results = FALSE;
			$this->errors[]="no_available_connection";
		}
		
		return $this->results;
	}
	
	function header_info() {
		$obj = imap_headerinfo($this->mbox,$this->msgno);
		
		//print_r($obj);
		
		$result = array(
						"date" => $obj -> date,
						"subject" => $obj -> subject,
						"message_id" => $obj -> message_id,
						"to" => $obj -> toaddress,
						"from" => $obj -> fromaddress,
						"replyto" => $obj -> reply_toaddress,
						"sender" => $obj -> senderaddress,
						"recent" => $obj -> Recent,
						"unseen" => $obj -> Unseen,
						"flagged" => $obj -> Flagged,
						"answered" => $obj -> Answered,
						"deleted" => $obj -> Deleted,
						"draft" => $obj -> Draft,
						"msgno" => $obj -> Msgno,
						"maildate" => $obj -> MailDate,
						"size" => $obj -> Size,
						"udate" => $obj -> udate,
						"cc" => $obj->ccaddress,
						);

		$this -> results = $result;
			
		return $this->results;
	}
	
	// returns the number of available folders
	function folders() {
		//$this -> __is_connected();
		//echo $this->connect_string;
		
		$raw_folders = imap_list($this->mbox, $this->connect_string, '*');
		
		foreach($raw_folders as $folder) {
			$folders[]= substr($folder, strlen($this->connect_string), strlen($folder)-strlen($this->connect_string));
		}
		
		$this->results = $folders;
		return $this->results;
		
		
	}
	


	
	// this returns the overview of all the messages in the folder
	// it returns an array
	function overview() {
		
		// very important this gets reset, otherwise it retuns an empty result
		unset($this->results);
		
		$check = imap_check($this->mbox);

		$messages = $check -> Nmsgs;

				
		if($messages<=0) return;
		
		//die('we are here');
		
		


		
		
		if($messages>0) {
			$overview = imap_fetch_overview($this->mbox, "1:".$messages."", 0);
		
			
			if(sizeof($overview)<=0) {
				//$this->results = NULL;
				return $this->results;	
				
			}
			

			
			foreach($overview as $index=>$obj) {

				if(!empty($obj)) {

					$haha[]= array(
											"subject" => $obj->subject,
											"from" => $obj->from,
											"to" => $obj->to,
											"date" => $obj->date,
											"message_id" => $obj->message_id,
											"size" => $obj->size,
											"uid" => $obj->uid,
											"msgno" => $obj->msgno,
											"recent" => $obj->recent,
											"flagged" => $obj-> flagged,
											"answered" => $obj -> answered,
											"deleted" => $obj -> deleted,
											"seen" => $obj -> seen,
											"draft" => $obj -> draft
											);
				}
				
			}
			
			$this->results = $haha;
		} 
		
		
		//print_r($haha);
		return $this->results;
	}
	
	
	function __get_mime_type(&$structure) {
   		$primary_mime_type = array("TEXT", "MULTIPART","MESSAGE", "APPLICATION", "AUDIO","IMAGE", "VIDEO", "OTHER");
   		
   		if($structure->subtype) {
   			return $primary_mime_type[(int) $structure->type] . '/' .$structure->subtype;
   		}

   		return "TEXT/PLAIN";
   }
   
	
   
	function __get_part($stream, $msg_number, $mime_type, $structure = false, $part_number    = false) {
		if(!$structure) {
   			$structure = imap_fetchstructure($stream, $msg_number);
   		}
   		
   		if($structure) {
   			if($mime_type == $this->__get_mime_type($structure)) {
   				if(!$part_number) {
   					$part_number = "1";
   				}
   				
   				$text = imap_fetchbody($stream, $msg_number, $part_number);
   				
   				if($structure->encoding == 3) {
   					return imap_base64($text);
   				} else if($structure->encoding == 4) {
   					return imap_qprint($text);
   				} else {
   					return $text;
   				}
   			}
   
			if($structure->type == 1) /* multipart */ {
   				while(list($index, $sub_structure) = each($structure->parts)) {
   					if($part_number) {
   						$prefix = $part_number . '.';
   					}
   					
   					$data = $this->__get_part($stream, $msg_number, $mime_type, $sub_structure,$prefix . ($index + 1));
   			
   					if($data) {
   						return $data;
   					}
   				} // END OF WHILE
   			} // END OF MULTIPART
   		} // END OF STRUTURE
   	return false;
   	
   }
   
   
  
   	
  	function get_message() {
  	
  		$message["header"]=$this->header_info();
  		$message["body"]["default"] = $this->__get_part($this->mbox,$this->msgno,"TEXT/PLAIN");

  		
  		$this->results = $message;
  		return $this->results;
    }	

    
    function add_folder() {
    	$results = imap_createmailbox($this->mbox, $this->connect_string.$this->folder);
    	$this -> results = $results;
    	return $this -> results;
    }
    
    
    function delete_folder() {
     	$results = imap_deletemailbox($this->mbox, $this->connect_string.$this->folder);
    	$this -> results = $results;
    	return $this -> results;   		
    }
    
	// closes the imap connection
	function close() {
		$this -> results = imap_close($this->mbox);
		return $this -> results;
	}

	function delete() {
		//echo "deleting message ".$this->msgno." from folder ".$this->folder;
		
		$this -> results = imap_delete($this->mbox,$this->msgno);
		

		
		return $this -> results;
	}
	
	function move() {
		if(empty($this->moveto_folder)) return FALSE;

		if(!preg_match("/\./", $this->moveto_folder)) {
			$this->moveto_folder = $this->default_folder.".".$this->moveto_folder;
		}
		
		//echo "moving ".$this->msgno." from ".$this->connect_string." to ".$this->moveto_folder."<br>\n";
		
		$this -> results = imap_mail_move($this->mbox, $this->msgno, $this->moveto_folder);

		//$this -> expunge();
		
		return $this -> results;
		
	
	}
	
	function expunge() {
		$this -> results = imap_expunge($this->mbox);
		return $this -> results;
		
	}

	
}


?>