<?php
function webmail_plus_fix_weight() {
  
  $file_weight = db_fetch_object(db_query("SELECT weight FROM {system} WHERE name='file' AND type='module'"));
  $file_attach_weight = db_fetch_object(db_query("SELECT weight FROM {system} WHERE name='file_attach' AND type='module'"));
  $webmail_plus_weight = db_fetch_object(db_query("SELECT weight FROM {system} WHERE name='webmail_plus' AND type='module'"));
  
//  echo 'file weight '.$file_weight->weight."<br>";
//  echo 'file attach weight '.$file_attach_weight->weight."<br>";
//  echo 'webmail plus weight '.$webmail_plus_weight->weight."<br>";
  
  
  //if($webmail_plus_weight->weight<$file_weight->weight && $webmail_plus_weight->weight>$file_attach_weight->weight) {
  if($file_weight->weight<=$file_attach_weight->weight+1) {
    drupal_set_message(t('Module weights on your site are set correctly. There\'s nothing to do.!'), 'notice', FALSE);
    return;
    
  }
  
  
  drupal_set_message(t('This feature is highly experimental, use it at your own risk!'), 'warning', FALSE);
  $form = array();
  $form['warning'] = array(
    '#type' => 'item',
    '#value' => t('By proceeding with this weights of File and File Attach modules will be modified. You should probably write them down, just in case. Currently File has a weight of %file_weight and File Attach has a weight of %file_attach_weight.', array('%file_weight'=>$file_weight->weight, '%file_attach_weight'=>$file_attach_weight->weight))
  );

  
  $form['submit'] = array(
    '#type' => 'submit',
    '#id' => 'submit',
    '#value' => t('Poof!!!')
  );
  
  $form['cancel'] = array(
    '#type' => 'submit',
    '#id' => 'cancel',
    '#value' => t('OMG I am scared. I want my mommy!')
  );
  
  return $form;
}

function webmail_plus_fix_weight_submit($form, &$form_state) {
  
  if($form_state['clicked_button']['#id']=='cancel') $form_state['redirect']='admin/build/modules';
  
  $file_weight = db_fetch_object(db_query("SELECT weight FROM {system} WHERE name='file' AND type='module'"));
  $file_attach_weight = db_fetch_object(db_query("SELECT weight FROM {system} WHERE name='file_attach' AND type='module'"));
  $webmail_plus_weight = db_fetch_object(db_query("SELECT weight FROM {system} WHERE name='webmail_plus' AND type='module'"));
  
  $new_file_weight = $webmail_plus_weight->weight+1;
  $new_file_attach_weight = $webmail_plus_weight->weight-1;
  
  db_query("UPDATE {system} SET weight=%d WHERE name='file' AND type='module'", $new_file_weight);
  db_query("UPDATE {system} SET weight=%d WHERE name='file_attach' AND type='module'", $new_file_attach_weight);
  
  drupal_set_message(t('Weights were adjusted successfully.'), 'warning', TRUE);
  
  $form_state['redirect']='admin/build/modules';
  //wpd($form_state);
  
}
?>