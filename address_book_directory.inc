<?php
function webmail_plus_address_book_directory_form() {
	global $ldap, $user;


	if($_POST['op']==t('Address Book')) {
		drupal_goto('webmail_plus/address_book');
	}

	$webmail_plus_config = __webmail_plus_get_config();
	$user_password = __webmail_plus_user_password();


	//print_r($webmail_plus_config);

	//$dn="uid=denis.voitenko,ou=lab.spawareurope.net,dc=spawareurope,dc=com";

	//$dn = $webmail_plus_config['ldap_user_attribute']."=".$user->name.",".$webmail_plus_config['ldap_base_db'];

	$dn = $webmail_plus_config['ldap_user_attribute'].'='.$user -> name.','.$webmail_plus_config['ldap_base_dn'];

	$ldap -> connect($dn, $user_password);

	$rs = $ldap -> search_list($webmail_plus_config['ldap_base_dn'], "objectClass=*", array('cn', 'mail'));

	$ldap -> disconnect();


	// clean up the result set, for some reason there's a count key in there, we dont want it
	unset($rs['count']);



	$form = array();

	$form['top_buttons']['address_book'] = array(
		'#type' => 'submit',
		'#value' => t('Address Book')
	);

	foreach($rs as $index=>$record) {

		$form['items'][$index]['name'] = array('#value' => $record['cn'][0]);
		$form['items'][$index]['email'] = array('#value' => $record['mail'][0]);


	}


	return drupal_get_form('webmail_plus_address_book_directory_form', $form);
}


function theme_webmail_plus_address_book_directory_form($form) {
	theme('add_style', drupal_get_path('module', 'webmail_plus') . '/webmail_plus.css');

	$output = form_render($form['top_buttons']);

	$headers  =  array(
	    array('data' => t('Name')),
	    array('data' => t('Email'))
	);

	$row = 0;
	foreach (element_children($form['items']) as $key) {
		$row++;

		if($row % 2 == 0) {
			$class = 'webmail_plus_row_even';
		} else {
			$class =  'webmail_plus_row_odd';
		}

		$rows[] = array(
			array('data'=>form_render($form['items'][$key]['name']),'class'=>$class),
			array('data'=>l(form_render($form['items'][$key]['email']), 'webmail_plus/compose/new/INDEX/0/'.$form['items'][$key]['email']['#value']),'class'=>$class),
		);


	}

	$output .= theme('table', $headers, $rows);
	return $output;
}

function webmail_plus_address_book_directory_form_submit($form_id, $form_values) {
	print_r($_POST['op']);
}

?>