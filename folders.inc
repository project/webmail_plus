<?

function webmail_plus_folder_delete($folder) {
  global $user, $webmail_plus_config, $mail_storage;

  if (!empty($folder)) {
    __webmail_plus_delete_folder($folder);
  }
  drupal_goto('webmail_plus/inbox/' . $_SESSION['webmail_plus_active_folder']);
}

// shows the list of folders
function webmail_plus_user_folders() {
	global $user, $webmail_plus_config;

	$folders = __webmail_plus_get_folders();

	/*
	$folders = __webmail_plus_get_folders_cache();

	// get folders directly form storage and update cache while we're at it
	if(sizeof($folders)<=0) {
		$content .= "folder cache was empty";

		$folders = __webmail_plus_get_folders_storage();
		$update = __webmail_plus_put_folders_cache($folders);
	}
	*/

	// create a form out of all folders
	$form = array();

	$form["add"] = array(
		"#type" => "fieldset",
		"#title" => t("Add a Folder"),
		"#collapsible" => FALSE,
		"#tree" => TRUE
	);

	$form["add"]["folder"] = array(
		"#type" => "textfield",
		"#title" => t("Folder Name"),
	);


	$form["add"]["submit"] = array(
		"#type" => "submit",
		"#value" => "Add"
	);

	$page = drupal_get_form('webmail_plus_folders_add_form', $form);

	unset($form);

	$required_folders = __webmail_plus_list_to_array($webmail_plus_config['required_folders'], TRUE);

	// sanitize folder list
	foreach($folders as $index=>$folder) {
		if($folder == "INBOX" || $folder == $webmail_plus_config['folder_prefix'] || $required_folders[$folder]) {
			unset($folders[$folder]);
		}
	}

	if(sizeof($folders)>0) {

		$form["delete"] = array(
			"#type" => "fieldset",
			"#title" => t("Delete Folder(s)"),
			"#collapsible" => FALSE,
			"#tree" => TRUE
		);


		// show them as content
		foreach($folders as $index=>$folder) {

			$form["delete"]['folders'][$folder] = array(
				"#type" => "checkbox",
				"#title" => t($folder),
				"#required" => FALSE
			);
		}

		$form["delete"]["submit"] = array("#type" => "submit", "#value" => t("Delete"));
		$page .= drupal_get_form('webmail_plus_folders_delete_form', $form);
		unset($form);
	}

	if(sizeof($folders)>0) {

	    // building the rename form
	    $form["rename"] = array("#type" => "fieldset", "#title" => t("Rename Folder"), "#collapsible" => FALSE, "#tree" => TRUE);
	    $form["rename"]["folders"] = array("#type" => "radios", "#options" => $folders);
	    $form["rename"]["newname"] = array('#type' => 'textfield', '#size' => 20, '#maxlength' => 255);
	    $form["rename"]["submit"] = array("#type" => "submit", "#value" => t("Rename"));
	    $page .= drupal_get_form('webmail_plus_folders_rename_form', $form);
	}

	return $page;
	//return drupal_get_form('webmail_plus_user_folders_form', $form);
}



function webmail_plus_folders_add_form_validate($form_id, $form_values) {
	$folder = trim($form_values['add']['folder']);

	//echo "folder: $folder<br>\n";

	if(preg_match("/[^a-z0-9\s]/i", $folder)) {
		form_set_error('',t('Folder name contains invalid characters. Only letters, numbers and spaces are allowed.'));
	}

	if($folder=='') {
		form_set_error('',t('Folder name is empty.'));
	}



}


function webmail_plus_folders_add_form_submit($form_id,$form_values) {
	global $webmail_plus_config;


	$folder = trim($form_values['add']['folder']);



	if(!empty($folder)) {

		$new_folder = __webmail_plus_add_folder($folder);

		//print_r($new_folder);
	}


	return 'webmail_plus/folders';

}


function webmail_plus_folders_delete_form_submit($form_id, $form_values) {

	//content.= "deleting a folder<br>\n";

	global $user, $webmail_plus_config, $mail_storage;

	foreach($form_values['delete']['folders'] as $folder => $checked) {

		if($checked==1) {

			__webmail_plus_delete_folder($folder);
		}

	}

	return 'webmail_plus/folders';
}

function webmail_plus_folders_rename_form_submit($form_id, $form_values) {
  global $user, $webmail_plus_config, $mail_storage;

  $oldname = $form_values['rename']['folders'];
  $newname = $form_values['rename']['newname'];

  if (empty($oldname) || empty($newname)) {
    drupal_set_message(t('Make sure you select a folder and enter a new name'));
    return 'webmail_plus/folders';
  }
  __webmail_plus_rename_folder($oldname, $newname);
  return 'webmail_plus/folders';
}

?>
