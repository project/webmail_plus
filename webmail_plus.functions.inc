<?php

function webmail_plus_get_profile_node_for($user=NULL) {
  if(!$user) global $user;
  
  if (module_exists('content_profile')) {
    foreach (content_profile_get_types('names') as $type => $type_name) {
      $profile = content_profile_load($type, $user->uid);
    }
    return $profile;
  }
}

/**
message subject should always be passed thru this function
since it ensures that if it's empty it still has some text
and thus will be clickable when turned into a link
*/
function _webmail_plus_render_from($from="") {
   $from=html_entity_decode(trim($from));
   if(empty($from)) return '('.t('No sender').')';
   
   
   $form=_webmail_plus_scrub_field($form);

   return _webmail_plus_extract_name($from);


}

function _webmail_plus_render_recepient($to="") {
   module_load_include('inc', 'webmail_plus', 'webmail_plus.charset_functions');
   
   $to=html_entity_decode(trim($to));
   $to=_webmail_plus_scrub_field($to);
   if(empty($to)) return '('.t('No recipient').')';
   
   
   return _webmail_plus_extract_name($to);
   


}

function _webmail_plus_render_status($status="") {
	if($status=="") return;
	
	if($status=="replied") return ' ['.t('Replied').']';
	
	if($status=="forwarded") return ' ['.t('Forwarded').']';
	
	return;
}


/**
message subject should always be passed thru this function
since it ensures that if it's empty it still has some text
and thus will be clickable when turned into a link
*/
function _webmail_plus_render_subject($subject="") {
  module_load_include('inc', 'webmail_plus', 'webmail_plus.charset_functions');
  
   $subject=html_entity_decode(trim($subject));
   if(empty($subject)) return t('(@nosubject)', array('@nosubject' => t('No subject')));

   //echo (decodeHeader($subject));
   
   $subject = html_entity_decode(decodeHeader($subject));
   return $subject;

}

/**
date should always be passed thru this function
since it ensures that if it's empty it still has some text
and thus will be clickable when turned into a link

additionally it makes the date more presentable
*/
function _webmail_plus_render_date($date="") {

   $date=trim($date);
   if(empty($date)) return t('(No date)');

   
   return format_date($date, 'custom', 'M j, Y g:i A');
}



/*
 * returns the profile stored in content_profile module
 */
function webmail_plus_profile_node_for($uid=NULL) {

  if(is_null($uid)) {
    global $user;
    $uid=$user->uid;
  }

  $object = user_load($uid);

  if (module_exists('content_profile')) {
    foreach (content_profile_get_types('names') as $type => $type_name) {
      $profile = content_profile_load($type, $user->uid);
    }
    return $profile;
  }
}

/**
 * takes an array of availlable folders and an optional array of hidden folders. compares the two and only returns the available ones
 *
 * @param unknown_type $available
 * @param unknown_type $hidden
 * @return unknown
 */
function __webmail_plus_resolve_folder_list($available, $hidden=NULL) {
  global $_webmail_plus_config;

  
  if(!$hidden) $hidden = $_webmail_plus_config -> hidden_folders;

  // if hidden is empty no reason to bother
  if(!$hidden) return $available;
  
  
  $hidden_folders_array_raw = explode(",", $hidden);
  foreach($hidden_folders_array_raw as $key=>$value) {
  	$hidden_folders_array[trim($value)]=trim($value);
  }
  
  foreach($available as $key=>$value) {
    $key=trim($key);
    $value=trim($value);
    if($hidden_folders_array[$value]) continue;
    
    $rs[$value]=$value;
  }
  
  return $rs;
}



function webmail_plus_parse_seconds($secs) {
  	// 604 800 week
  	// 86 400 day
  	// 3600
  	// 60 minute
  	

  $units = array(
    "weeks"   => 7*24*3600,
    "days"    =>   24*3600,
    "hours"   =>      3600,
    "minutes" =>        60,
    "seconds" =>         1,
  );
  
  foreach ( $units as &$unit ) {
    $quot  = intval($secs / $unit);
    $secs -= $quot * $unit;
    $unit  = $quot;
  }

  return $units;
}
	

 

/**
 * returns a human-readable string of weeks, days, hours, minutes
 *
 * @param unknown_type $seconds
 * @return unknown
 */
function webmail_plus_seconds_to_human($seconds, $complete=FALSE) {
	
	$set = webmail_plus_parse_seconds($seconds);
	
	if($complete) {
		foreach($set as $key=>$value) {
			if($value<=0) continue;
			
			$rs[]=$value.' '.t($key);
		}

		return implode(', ', $rs);
		
	}
	

	if($set['weeks']>0) {
		if($set['weeks']==1) {
			return $set['weeks'].' '.t('week');
		}
		else {
			return $set['weeks'].' '.t('weeks');
		}
	}

	if($set['days']>0) {
		if($set['days']==1) {
			return $set['days'].' '.t('day');
		}
		else {
			return $set['days'].' '.t('days');
		}
	}
	
	if($set['hours']>0) {
		if($set['hours']==1) {
			return $set['hours'].' '.t('hour');
		}
		else {
			return $set['hours'].' '.t('hours');
		}
	}
	
	if($set['minutes']>0) {
	  if($set['minutes']==1) {
      return $set['minutes'].' '.t('minute');
    }
    else {
      return $set['minutes'].' '.t('minutes');
    }		
	}
	
	return t('less than a minute');

}

/**
 * takes a message_key as an argument and deletes that message
 *
 * @param unknown_type $message_key
 * @param unknown_type $user
 */
function _webmail_plus_move_message($message_key, $destination, $user=NULL) {
  if(!$user) global $user;
  global $_webmail_plus_config;
  
  //drupal_set_message("moving message $message_key to $destination");
  
  
  module_load_include('inc', 'webmail_plus', 'webmail_plus.tag_functions');
  
      
  $message_parts = webmail_plus_parse_message_key($message_key);
  _webmail_plus_connect($message_parts->folder);
  
  $message_header = webmail_plus_parse_header(mail_api_header($message_parts->message_uid), TRUE);
  $message_header = (object)$message_header;
  
  mail_api_move_message($message_parts->message_uid, $destination);
  
  // now lets track that message and tag it
  _webmail_plus_connect($destination);     
  $overview = mail_api_overview();
  


  if(sizeof($overview)<=0) return; // something probably went wrong but it's not critical, we just lose a tag
  // since the last message will be closer to the top, reverse the array
  $overview=array_reverse($overview);

  // match headers
  foreach($overview as $id=>$message_info) {
    if(_webmail_plus_compare_headers($message_header, $message_info)) {
      //die("headers matched, remapping tags<br>\n");
      
      _webmail_plus_remap_tags($message_key, $user->uid.'!'.$destination.'@'.$message_info->uid);
      break;
    }

  }
  
}

function _webmail_plus_compare_headers($header1,$header2,$fields='from,to,subject,date') {

  
  $fields=trim($fields);
  if($fields=='') return FALSE;

  $fields_array=preg_split("/,/", $fields);
  $fields_proper = array_flip($fields_array);

  foreach($fields_array as $field_id=>$field) {
    $field=strtolower($field);
    //echo "checking field $field: ".html_entity_decode($header1->$field)." vs ".html_entity_decode($header2->$field)."<br>\n";

    if(html_entity_decode($header1->$field)!=html_entity_decode($header2->$field)) return FALSE;
  }


  return TRUE;
  
}



/**
 * takes a message_key as an argument and deletes that message
 *
 * @param unknown_type $message_key
 * @param unknown_type $user
 */
function _webmail_plus_delete_message($message_key, $user=NULL) {
  if(!$user) global $user;
  global $_webmail_plus_config;
  
  $message_parts = webmail_plus_parse_message_key($message_key);
  _webmail_plus_connect($message_parts->folder);
  mail_api_delete_message($message_parts->message_uid);
}

function _webmail_plus_get_user_folders($cache=TRUE, $user=NULL) {
  if(!$user) global $user;
  global $_webmail_plus_config;

  // if the flag isn't set we load the folders from the mail api even if the user specified cache
  if($_SESSION['webmail_plus']['connection_disabled']) return FALSE;
  if(!$_SESSION['webmail_plus']['folders_loaded']) $cache=FALSE;
  
  // create a list of required folders
  $required_folders_array = explode(",", $_webmail_plus_config -> required_folders);
  if(is_array($required_folders_array)) {
	  foreach($required_folders_array as $index=>$folder_name) {
	  	$required_folders[trim($folder_name)]=trim($folder_name);
	  }
  }
  else {
  	$required_folders = array();
  }
  
  
  // get them from cache
  if($cache) {
  	$results = db_query("SELECT `folder` FROM {webmail_plus_user_folders} WHERE uid=%d", $user->uid);
  	while($folder = db_fetch_object($results)) {
  		$output[$folder->folder]=$folder->folder;
  	}
  }
  else {
  	//echo "loading from mailapi";
  	
  	// fetch folders from mail_api and store them in cache
  	$mail_api_connection = _webmail_plus_connect();
  	$mailboxes = __webmail_plus_resolve_folder_list(mail_api_mailboxes());
    //wpd($mailboxes);
  	
  	// delete all folders
  	db_query("DELETE FROM {webmail_plus_user_folders} WHERE uid=%d", $user->uid);
  	
    	
    if (is_array($mailboxes)) {
      foreach($mailboxes as $mailbox_id => $mailbox_name) {
        if ($mailbox_name=="") continue;
        db_query("INSERT INTO {webmail_plus_user_folders}(uid,folder) VALUES(%d,'%s')", $user->uid, $mailbox_name);
        
        $output[$mailbox_name]=$mailbox_name;
     }
    }  	
    
    // set the session to true
    $_SESSION['webmail_plus']['folders_loaded']=TRUE;
  }
  
  
  // now translate folders if needed
/*
  if(is_array($output)) {
    foreach($output as $folder_name=>$folder_junk) {
    	//if($required_folders[$folder_name]) 
    	$output[$folder_name]=t($folder_name);
    }
  }
*/
  
  return $output;
}

function _webmail_plus_get_newer_message($timestamp, $folder=NULL, $user=NULL) {
	if(!$user) global $user;
	if(!$folder) return FALSE;
	
	$message_key=db_result(db_query("SELECT `message_key` FROM {webmail_plus_user_headers} WHERE `date`>%d AND uid=%d AND `folder`='%s' ORDER BY `date` ASC LIMIT 1", $timestamp, $user->uid, $folder));
	if(!$message_key) {
  	return FALSE;
  }
  else {
  	return $message_key;
  }
}

function _webmail_plus_get_older_message($timestamp, $folder=NULL, $user=NULL) {
  if(!$user) global $user;
  if(!$folder) return FALSE;
  
  $message_key=db_result(db_query("SELECT `message_key` FROM {webmail_plus_user_headers} WHERE `date`<%d AND uid=%d AND `folder`='%s' ORDER BY `date` DESC LIMIT 1", $timestamp, $user->uid, $folder));
  if(!$message_key) {
    return FALSE;
  }
  else {
    return $message_key;
  }	
}

function _webmail_plus_parse_email($email) {
  if(_webmail_plus_extract_email($email)) $rs['email']=_webmail_plus_extract_email($email);
  
  list($first_name, $last_name) = split(" ", _webmail_plus_extract_name($email));
  if(trim($first_name)) $rs['first_name']=trim($first_name);
  if(trim($last_name)) $rs['last_name']=trim($last_name);
    
    
  $rs['user']=_webmail_plus_extract_user($rs['email']);
  $rs['domain']=_webmail_plus_extract_domain($rs['email']);
    
 
  return $rs;
  
}

function _webmail_plus_extract_email($email) {
	$email=trim($email);
	if(empty($email)) return FALSE;

	if(strpos($email, "&lt;") || strpos($email, "&gt;")) {
		$email = preg_replace("/\&lt\;/", "<", $email);
		$email = preg_replace("/\&gt\;/", ">", $email);
	}


	preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/", $email, $matches);
	
    $email = $matches[0];
    return $email;
}

/*
takes an email as an input and returns just the user part, removing the domain
*/
function _webmail_plus_extract_user($email) {

	if(!_webmail_plus_email_valid($email)) {
		return FALSE;
	}

	list($username,$domain) = split("@", $email);

	return $username;


}

function _webmail_plus_extract_domain($email) {

	if(!_webmail_plus_email_valid($email)) {
		return FALSE;
	}

	list($username,$domain) = split("@", $email);

	return $domain;


}
/*
tries to extract the name from the email
if fails returns the email
*/
function _webmail_plus_extract_name($email, $use_me=TRUE) {
  global $user, $_webmail_plus_config;
  
  $email = trim($email);
  
  $email = _webmail_plus_present_email($email);


  
  if(_webmail_plus_extract_email($email) == $user->name."@".$_webmail_plus_config -> domain  && $use_me) return 'me';
  
  
  
	    
	if(preg_match("/</", $email) && preg_match("/>/", $email)) {
		$name = substr($email,0,strpos($email,"<"));

		if(preg_match("/\"/", $name)) {
			$name = preg_replace("/\"/", "", $name);
			$name = trim($name);
		}

		
		
		if($name[0]=="\"") $name=substr($name,1, strlen($name)-1);
		if($name[strlen($name)-1]=="\"") $name=substr($name,0, strlen($name)-1);
		
		
		return $name;

	} 
	else {
	  list($username, $domain) = split("@", $email);
	  return $username;
	}
	
	return;
}

function _webmail_plus_present_email($email) {
	$email = preg_replace("/\&lt\;/", "<", $email);
	$email = preg_replace("/\&gt\;/", ">", $email);
	$email = preg_replace("/&quot;/", "\"", $email);
	return $email;
}

function _webmail_plus_clean_email($email) {
  if(preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/", $email)) return TRUE;
  return FALSE;
}


function _webmail_plus_email_valid($email) {

	if(preg_match("/(.*)\<(.*)\>/", $email)) {
		preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/", $email, $matches);
		$email = $matches[0];
	}

  //echo "email $email<br>\n";

	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$",$email)) {
		//echo "email $email is invalid<br>\n";
		return FALSE;
	} else {
		//echo "email $email is valid<br>\n";
		return TRUE;
	}

}

function _webmail_plus_check_email_address($email) {
  // First, we check that there's one @ symbol, and that the lengths are right
  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
    // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
    return false;
  }
  
  // Split it into sections to make life easier
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++) {
    if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
    return false;
    }
  }
  
  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
    $domain_array = explode(".", $email_array[1]);
    if (sizeof($domain_array) < 2) {
      return false; // Not enough parts to domain
    }
  
    for ($i = 0; $i < sizeof($domain_array); $i++) {
      if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
        return false;
      }
    }
  }
  
  return true;
}

/**
 * generates a link that will be used to add an email address to the contact manager from the header
 *
 * @param unknown_type $email
 */
function _webmail_plus_add_to_address_book($email) {
  global $user, $_webmail_plus_config;

  $just_email = _webmail_plus_extract_email($email);
  
  if($just_email==$user->name.'@'.$_webmail_plus_config -> domain) return;
  
  // if the email already exists, skip it

  if(module_exists('contact_manager')) {
    if(contact_manager_contact_exists($user, 'field_email', $just_email)) return;
  }

  
  $name = _webmail_plus_extract_name($email);


  if(strpos($name, " ")>0) {
    list($first_name, $last_name) = explode(' ', $name, 2);
  }
  else {
    $first_name = $name;
  }
  
  if($first_name && $last_name) {
    return l(t('(!add)', array('!add'=>t('Add'))), 'node/add/contact-manager', array('query' => array('field_email' => $just_email, 'field_first_name' => $first_name, 'field_last_name' => $last_name)));
  }

  return l(t('(!add)', array('!add'=>t('Add'))), 'node/add/contact-manager', array('query' => array('field_email' => $just_email)));
}

function _webmail_plus_present_email_list($list) {
  global $user;
  
  if($list=="") return;
  
  $emails = explode(", ", $list);
  
  foreach($emails as $index=>$email) {
    if(_webmail_plus_add_to_address_book($email)) {
      $pieces[]=_webmail_plus_make_clickable($email).' '._webmail_plus_add_to_address_book($email);
      
    }
    else {
      $pieces[]=$email;
    }
  }
  
  return implode(", ", $pieces);
}

function _webmail_plus_get_header_count($folder="INBOX", $field, $value, $user=NULL) {
  if(!$user) global $user;
  if((string)$field=='' || (string)$value=="") return;

  
  //echo $field." ".$value."<br>\n";
  
  $count=db_result(db_query("SELECT COUNT(*) FROM {webmail_plus_user_headers} WHERE uid=%d AND folder='%s' AND %s='%s'", $user->uid,$folder, $field, $value));
  
  return $count;
}

/**
 * returns a list of invalid addresses
 *
 * @param unknown_type $list
 * @return unknown
 */
function _webmail_plus_validate_email_list($list, $array=TRUE) {
	$emails = explode(",", $list);
	if(!is_array($emails)) return FALSE;
	
	// cleanup emails
	foreach($emails as $index=>$email) {
	  $email = trim($email);
	  if($email=="") continue;
	  $emails[$index]=$email;
	}
	
	// validate emails
	foreach($emails as $index=>$email) {
	  
	  if(trim($email)=="") continue;
	  if(!_webmail_plus_email_valid($email)) $invalid[]=$email;
	}
	
	

	
	if($array) {
	 return $invalid;
	}
	else {
		return implode(", ", $invalid); 
	}
}

function _webmail_plus_cleanup_email_list($list, $array=TRUE) {
  $emails = explode(",", $list);

  
  if(!is_array($emails)) return FALSE;
  
  foreach($emails as $index=>$email) {
    $email = trim($email);
	if($email=="") continue;
	$rs[]=$email;
  }
  
  $rs = array_unique($rs);
  
  

  
  if($array) {
    return $rs;
  }
  else {
    return implode(", ", $rs); 
  }
  
}

function _webmail_plus_scrub_field($field) {
  if($field=='undisclosed-recipients:' || $field=='undisclosed-recipients:;') return "";
  return $field;  
}








function _webmail_plus_flag_read($message_key) {
  if(!$message_key) return;
  //$message_parts = webmail_plus_parse_message_key($message_checked);
  db_query("UPDATE {webmail_plus_user_headers} SET seen=1 WHERE message_key='%s'", $message_key);
  
}

function _webmail_plus_flag_unread($message_key) {
  if(!$message_key) return;
  //$message_parts = webmail_plus_parse_message_key($message_checked);
  db_query("UPDATE {webmail_plus_user_headers} SET seen=0 WHERE message_key='%s'", $message_key);
  
}


// adds a bit of personalization to the presentation of addressee
function _webmail_plus_combine_to_from($to=NULL,$from=NULL) {
  
  // Per our chat, please special-case it as '@from > me', 'me > me', 'me > @to' 
  
  if($to && !$from) {
    return _webmail_plus_scrub_field(_webmail_plus_extract_name($to));
  }
  
  if(!$to && $from) {
    return _webmail_plus_scrub_field(_webmail_plus_extract_name($from));
  }
  
  if(_webmail_plus_extract_name($from)=='me' && _webmail_plus_extract_name($to)=='me') {
    return t('me > me');
  }
  
  if(_webmail_plus_extract_name($from)=='me') {
    $to = _webmail_plus_scrub_field(_webmail_plus_extract_name($to));
    return t('me > !to', array('!to'=>$to));
  }

  if(_webmail_plus_extract_name($to)=='me') {
    $from = _webmail_plus_scrub_field(_webmail_plus_extract_name($from));
    return t('!from > me', array('!from'=>$from));
  }
  
  //return _webmail_plus_scrub_field(_webmail_plus_extract_name($from))." > "._webmail_plus_scrub_field(_webmail_plus_extract_name($to));  
}


function _webmail_plus_order_folders($mailboxes) {
  global $_webmail_plus_config;
  if(!$_webmail_plus_config->folder_order) return $mailboxes;
  
  // split it into an array
  $folder_order_arr = explode(", ", $_webmail_plus_config->folder_order);
  foreach($folder_order_arr as $key=>$value) {
    $folder_order_sorted[trim($value)]=trim($value);
    
    if($mailboxes[trim($value)]) {
      $rs[trim($value)]=$mailboxes[trim($value)];
    }
    
  }
  


  if(sizeof($mailboxes)>0) {
    foreach($mailboxes as $key=>$value) {
      if($rs[$key]) continue;
      $rs[$key]=$value;
    }
  }
  else {
    $rs=array();
  }

  return $rs;
    
}


function _webmail_plus_get_autocomplete_url() {
  global $_webmail_plus_config;
  
  if(!$_webmail_plus_config->address_book) return FALSE;
  
  if(!module_exists($_webmail_plus_config->address_book)) return FALSE;
  
  if($_webmail_plus_config->address_book=='contact_manager') {
    return 'contact_manager/search/autocomplete';
  }
}

function _webmail_plus_message_exists($user, $folder, $message_uid) {
  if(!$user) global $user;
  
  $result = db_fetch_object(db_query("SELECT COUNT(*) AS count FROM {webmail_plus_user_headers} WHERE uid=%d AND folder='%s' AND message_uid=%d", $user->uid, $folder, $message_uid));

  $count = $result->count;
  if($count>0) {
    return TRUE;
  }
  else {
    return FALSE;
  }

}

function _webmail_plus_message_count($user, $folder="INBOX") {
  if(!$user) global $user;
  $result = db_fetch_object(db_query("SELECT COUNT(*) AS count FROM {webmail_plus_user_headers} WHERE uid=%d AND folder='%s'", $user->uid, $folder));
  return $result->count;    
}


/*
 * clear user-related stuff when a user is deleted
 */
function webmail_plus_user($op, &$edit, &$account, $category = NULL) {

  $webmail_plus_tables = array(
    'webmail_plus_user_cron',
    'webmail_plus_user_filters',
    'webmail_plus_user_folders',
    'webmail_plus_user_headers',
    'webmail_plus_user_headers_by_tag',
    'webmail_plus_user_passwords',
    'webmail_plus_user_preferences',
    'webmail_plus_user_signatures',
    'webmail_plus_user_tags',
    'webmail_plus_user_threads',
    'webmail_plus_user_threads_map'
  );
  
  if($op=='delete') {
    
    foreach($webmail_plus_tables as $id=>$table) {
      db_query("DELETE FROM {%s} WHERE uid=%d", $table, $edit->uid);
    }
  }
  
}





function webmail_plus_set_user_password($password, $user=NULL) {
  if(!$user) global $user;
  
  $password=trim($password);
  if($password=="") return FALSE;
  
  if(module_exists('webmail_plus_crypt')) {
    $password = webmail_plus_crypt_encrypt($password);
  }
  
  // figure out if the variable was already set
  $count = db_result(db_query("SELECT COUNT(password) FROM {webmail_plus_user_passwords} WHERE uid=%d", $user->uid));

  if($count<=0) {
    db_query("INSERT INTO {webmail_plus_user_passwords}(uid, `password`) VALUES(%d, '%s')", $user->uid, $password);
  }
  else {
    db_query("UPDATE {webmail_plus_user_passwords} SET `password`='%s' WHERE uid=%d", $password, $user->uid);
  }  

}

function _webmail_plus_escape_reserved_db_words($string) {
  $string = trim($string);
  $string = preg_replace("/to/", "`to`", $string);
  $string = preg_replace("/from/", "`from`", $string);
  $string = preg_replace("/date/", "`date`", $string);
  return $string;  
}

/**
 * marks a message as read, but only in the database
 *
 * @param unknown_type $folder
 * @param unknown_type $message_uid
 */
function webmail_plus_thread_flag($folder, $tid, $flag, $value) {
  global $user;


  if($folder=="" || $tid=="" || $flag=="" || $value=="") return FALSE;
  //drupal_set_message("folder $folder tid $tid flag $flag value $value");


  // mark all messages in the tread with this
  $result = db_query("SELECT * FROM {webmail_plus_user_threads_map} WHERE uid=%d AND folder='%s' AND tid=%d", $user->uid, $folder, $tid);
  while($record = db_fetch_object($result)) {
    webmail_plus_message_flag($record->folder, $record->message_uid, $flag, $value);
  }

  // and mark the thread itself
  db_query("UPDATE {webmail_plus_user_threads} SET %s=%s WHERE uid=%d AND folder='%s' AND tid=%d", $flag, $value, $user->uid, $folder, $tid);

  return TRUE;
}

function webmail_plus_parse_message_key($message_key, $object=TRUE) {
  if(!preg_match("/^[0-9]{1,6}\![\w-_\.\s]{1,128}@\w{1,6}$/", $message_key)) {
    return false;
  }


  preg_match("/^([0-9]{1,6})\!([\w-_\.\s]{1,128})@(\w{1,6})$/", $message_key, $matches);

  $rs['uid']=$matches[1];
  $rs['folder']=$matches[2];
  $rs['message_uid']=$matches[3];


  if($object) {
    return webmail_array_to_object($rs);
  }
  else {
    return $rs;
  }

}

function webmail_plus_parse_thread_key($message_key, $object=TRUE) {
  if(!preg_match("/^[0-9]{1,6}\\$[\w-_\.\s]{1,128}@\w{1,6}$/", $message_key)) {
    return false;
  }


  preg_match("/^([0-9]{1,6})\\$([\w-_\.\s]{1,128})@(\w{1,6})$/", $message_key, $matches);



  $rs['uid']=$matches[1];
  $rs['folder']=$matches[2];
  $rs['thread_id']=$matches[3];


  if($object) {
    return webmail_array_to_object($rs);
  }
  else {
    return $rs;
  }

}

/**
 * marks a message as read, but only in the database
 *
 * @param unknown_type $folder
 * @param unknown_type $message_uid
 */
function webmail_plus_message_flag($folder, $message_uid, $flag, $value) {

  //drupal_set_message("setting flag $flag on $folder on $message_uid");
  global $user;

  if($folder=="" || $message_uid=="" || $flag=="" || $value=="") return FALSE;

  db_query("UPDATE {webmail_plus_user_headers} SET %s=%s WHERE uid=%d AND folder='%s' AND message_uid=%d", $flag, $value, $user->uid, $folder, $message_uid);

  return TRUE;
}






/**
 * automatically detects the key type and returns a parsed object
 *
 * @param unknown_type $key
 * @return unknown
 */
function webmail_plus_parse_key($key) {
  
  $type = webmail_plus_message_type($key);
  
  switch($type) {
    case 'message':
      return webmail_plus_parse_message_key($key);
    break;
    
    case 'thread':
      return webmail_plus_parse_thread_key($key);
    break;
    
    default:
      return FALSE;
    break;
      
  }
  
}

/**
 * takes a message key and returns either message or thread
 *
 * @param unknown_type $key
 * @return unknown
 */
function webmail_plus_message_type($key) {

  if(preg_match("/^[0-9]{1,6}\\$[\w-_\.\s]{1,128}@\w{1,6}$/", $key)) {
    return "thread";
  }
  else if(preg_match("/^[0-9]{1,6}\![\w-_\.\s]{1,128}@\w{1,6}$/", $key)) {
    return "message";
  }

  return "unknown";
}

/**
 * a little helper function to quickly pull some stats out of folders
 *
 * @param unknown_type $folder
 * @param unknown_type $flag
 * @param unknown_type $value
 */
function webmail_plus_folder_stats($folder, $flag, $value) {
  global $user;



  if($folder=="" || $flag=="" || (string)$value=="") return FALSE;


  $result = db_result(db_query("SELECT COUNT(*) FROM {webmail_plus_user_headers} WHERE folder='%s' AND uid=%d AND %s='%s'", $folder, $user->uid, $flag, $value));

  return $result;
}



/*
takes input in bytes and returns megabytes, kilobytes
*/
function _webmail_plus_reformat_size($bytes) {
  if($bytes>=1048576) {
    $rs=round($bytes/1048576, 2);
    $units = 'Mb';
  } elseif($bytes>=1024 && $bytes<1048576) {
    $rs = round($bytes/1024, 2);
    $units = 'Kb';
  } else {
    $rs = $bytes;
    $units = 'Bytes';
  }

  return $rs.' '.$units;
}


function _webmail_plus_create_directory($path) {
  
  if(file_exists($path)) {
    if(chmod($path, 0777)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  else {
  
    if(mkdir($path,0777,TRUE)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
}

function _webmail_plus_make_clickable($text)
{
	//$text = htmlspecialchars_decode ($text);
	//print_r($text);
	
  # this functions deserves credit to the fine folks at phpbb.com

  $text = preg_replace('#(script|about|applet|activex|chrome):#is', "\\1:", $text);

  // pad it with a space so we can match things at the start of the 1st line.
  $ret = ' ' . $text;

  // matches an "xxxx://yyyy" URL at the start of a line, or after a space.
  // xxxx can only be alpha characters.
  // yyyy is anything up to the first space, newline, comma, double quote or <
  $ret = preg_replace("#(^|[\n ])([\w]+?://[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a target=new href=\"\\2\" target=\"_blank\">\\2</a>", $ret);

  // matches a "www|ftp.xxxx.yyyy[/zzzz]" kinda lazy URL thing
  // Must contain at least 2 dots. xxxx contains either alphanum, or "-"
  // zzzz is optional.. will contain everything up to the first space, newline,
  // comma, double quote or <.
  $ret = preg_replace("#(^|[\n ])((www|ftp)\.[\w\#$%&~/.\-;:=,?@\[\]+]*)#is", "\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>", $ret);

  // matches an email@domain type address at the start of a line, or after a space.
  // Note: Only the followed chars are valid; alphanums, "-", "_" and or ".".
  //$ret = preg_replace("#(^|[\n ])([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "\\1<a href=\"".base_path().WEBMAIL_PLUS_ALIAS."/compose?to=\\2@\\3\">\\2@\\3</a>", $ret);

  
  $ret = preg_replace("#([a-z0-9&\-_.]+?)@([\w\-]+\.([\w\-\.]+\.)*[\w]+)#i", "<a href=\"".base_path().WEBMAIL_PLUS_ALIAS."/compose?to=\\1@\\2\">\\1@\\2</a>", $ret);
    
  
  // Remove our padding..
  $ret = substr($ret, 1);
  return $ret;
}

function webmail_plus_strip_tags_attributes($string,$allowtags=NULL,$allowattributes=NULL){
    $string = strip_tags($string,$allowtags);
    if (!is_null($allowattributes)) {
        if(!is_array($allowattributes))
            $allowattributes = explode(",",$allowattributes);
        if(is_array($allowattributes))
            $allowattributes = implode(")(?<!",$allowattributes);
        if (strlen($allowattributes) > 0)
            $allowattributes = "(?<!".$allowattributes.")";
        $string = preg_replace_callback("/<[^>]*>/i",create_function(
            '$matches',
            'return preg_replace("/ [^ =]*'.$allowattributes.'=(\"[^\"]*\"|\'[^\']*\')/i", "", $matches[0]);'   
        ),$string);
    }
    return $string;
} 

function _webmail_plus_set_message_status($message_key=NULL, $status=NULL) {
	global $user;

	//drupal_set_message("setting status $status for $message_key");
	
	if(!$message_key || !$status) return;
	
	$message_parts = webmail_plus_parse_message_key($message_key);
	
	  
	db_query("INSERT INTO {webmail_plus_user_message_status}(uid, folder, message_uid, message_key, status) VALUES(%d, '%s', '%s', '%s', '%s')", $message_parts->uid, $message_parts->folder, $message_parts->message_uid, $message_key, $status );

	return;
}

function _webmail_plus_get_message_status($message_key) {
	return db_fetch_object(db_query("SELECT * FROM {webmail_plus_user_message_status} WHERE message_key='%s'", $message_key));
}

function _webmail_plus_delete_message_status($message_key) {
  return db_query("DELETE FROM {webmail_plus_user_message_status} WHERE message_key='%s'", $message_key);
}
