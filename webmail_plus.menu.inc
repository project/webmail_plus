<?php
function webmail_plus_menu_run() {
	
  $items['admin/settings/webmail_plus'] = array(
   'title' => 'Webmail Plus',
   'description' => 'Settings for Webmail Plus.',
   'access arguments' => array('administer webmail_plus'),
   'page callback' => 'drupal_get_form',
   'page arguments' => array('webmail_plus_admin_settings'),
   'file' => 'webmail_plus.admin.inc',
  );


  $items[WEBMAIL_PLUS_ALIAS] = array(
   'title' => 'Webmail Plus',
   'description' => 'Entry point into Webmail Plus',
   'access arguments' => array('access webmail_plus'),
   'page callback' => 'drupal_get_form',
   'page arguments' => array('webmail_plus_gateway'),
   'file' => 'webmail_plus.gateway.inc',
   'type' => MENU_CALLBACK,
  );



  $items[WEBMAIL_PLUS_ALIAS.'/view_folder'] = array(
   'title' => 'Webmail Plus',
   'description' => 'Settings for Webmail Plus.',
   'access arguments' => array('access webmail_plus'),
   'page callback' => 'drupal_get_form',
   'page arguments' => array('webmail_plus_view_folder_form'),
   'file' => 'webmail_plus.view_folder.inc',
   'type' => MENU_CALLBACK,
  );

  $items[WEBMAIL_PLUS_ALIAS.'/reconnect'] = array(
   'title' => 'Webmail Plus',
   'description' => 'Settings for Webmail Plus.',
   'access arguments' => array('access webmail_plus'),
   'page callback' => 'drupal_get_form',
   'page arguments' => array('webmail_plus_reconnect'),
   'file' => 'webmail_plus.reconnect.inc',
   'type' => MENU_CALLBACK,
  );
  
  $items[WEBMAIL_PLUS_ALIAS.'/view_threads'] = array(
   'title' => 'Webmail Plus',
   'description' => 'View Messages by Threads.',
   'access arguments' => array('access webmail_plus'),
   'page callback' => 'drupal_get_form',
   'page arguments' => array('webmail_plus_view_threads_form'),
   'file' => 'webmail_plus.view_threads.inc',
   'type' => MENU_CALLBACK,
  );

  $items[WEBMAIL_PLUS_ALIAS.'/view_tag'] = array(
    'title' => 'View Messages by Tags',
    'page callback' => 'webmail_plus_view_tag',
    'access arguments' => array('access webmail_plus'),
    'file' => 'webmail_plus.view_tag.inc',
    'type' => MENU_CALLBACK,
  );
  
  $items[WEBMAIL_PLUS_ALIAS.'/delete_tag'] = array(
    'title' => 'D',
    'page callback' => 'webmail_plus_delete_tag',
    'access arguments' => array('access webmail_plus'),
    'file' => 'webmail_plus.delete_tag.inc',
    'type' => MENU_CALLBACK,
  );  
  
  $items[WEBMAIL_PLUS_ALIAS.'/manage_tags'] = array(
    'title' => 'Manage Tags',
    'page callback' => 'webmail_plus_manage_tags',
    'access arguments' => array('access webmail_plus'),
    'file' => 'webmail_plus.manage_tags.inc',
    'type' => MENU_CALLBACK,
  );

  $items[WEBMAIL_PLUS_ALIAS.'/compose'] = array(
    'title' => 'Compose a New Message',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webmail_plus_compose_form'),
    'access arguments' => array('access webmail_plus'),
    'type' => MENU_CALLBACK,
    'file' => 'webmail_plus.compose.inc',
  
  );
  
  
  $items[WEBMAIL_PLUS_ALIAS.'/message_view'] = array(
    'title' => 'View Email Message',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webmail_plus_message_view_form'),
    'access arguments' => array('access webmail_plus'), 
    'type' => MENU_LOCAL_TASK,
    'file' => 'webmail_plus.message_view.inc',  
  );

  
  $items[WEBMAIL_PLUS_ALIAS.'/download_attachment'] = array(
    'title' => 'Downlaod Attachment',
    'page callback' => 'webmail_plus_download_attachment',
    'access arguments' => array('access webmail_plus'),
    'file' => 'webmail_plus.download_attachment.inc',
    'type' => MENU_CALLBACK,
  );
  
  $items[WEBMAIL_PLUS_ALIAS.'/thread_view'] = array(
    'title' => 'View Email Messages in Threads',
    'page callback' => 'webmail_plus_thread_view',
    'access arguments' => array('access webmail_plus'),
    'file' => 'webmail_plus.thread_view.inc',
    'type' => MENU_CALLBACK,
  );


  $items[WEBMAIL_PLUS_ALIAS.'/tag_autocomplete'] = array(
    'title' => 'Tag autocomplete',
    'page callback' => 'webmail_plus_tag_autocomplete',
    'access arguments' => array('access webmail_plus'),
    'type' => MENU_CALLBACK,
    'file' => 'webmail_plus.tag_autocomplete.inc',
  );

  $items[WEBMAIL_PLUS_ALIAS.'/tag_list_autocomplete'] = array(
    'title' => 'Tag list autocomplete',
    'page callback' => 'webmail_plus_tag_list_autocomplete',
    'access arguments' => array('access webmail_plus'),
    'type' => MENU_CALLBACK,
    'file' => 'webmail_plus.tag_list_autocomplete.inc',
  );

  $items[WEBMAIL_PLUS_ALIAS.'/address_autocomplete'] = array(
    'title' => 'All users of the site',
    'page callback' => 'webmail_plus_address_autocomplete',
    'access arguments' => array('access webmail_plus'),
    'type' => MENU_CALLBACK,
    'file' => 'webmail_plus.address_autocomplete.inc',
  );  

  $items['user/%user/'.WEBMAIL_PLUS_ALIAS] = array(
    'title' => 'Email',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webmail_plus_preferences_presentation',1),
    'access callback' => 'user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'webmail_plus.user_preferences_presentation.inc',
  );


  $items['user/%user/'.WEBMAIL_PLUS_ALIAS.'/presentation'] = array(
    'title' => 'Presentation',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webmail_plus_preferences_presentation',1),
    'access callback' => 'user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'webmail_plus.user_preferences_presentation.inc',
    'weight' => -10,
  );

  $items['user/%user/'.WEBMAIL_PLUS_ALIAS.'/signature'] = array(
    'title' => 'Signature',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webmail_plus_preferences_signature',1),
    'access callback' => 'user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'webmail_plus.user_preferences_signature.inc',
    'weight' => -10,
  );

  $items['user/%user/'.WEBMAIL_PLUS_ALIAS.'/filters'] = array(
    'title' => 'Email Filters',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webmail_plus_preferences_filters'),
    'access callback' => 'user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'webmail_plus.user_preferences_filters.inc',
    'weight' => -9,
  );
  
  $items['user/%user/'.WEBMAIL_PLUS_ALIAS.'/filters/add'] = array(
    'title' => 'Email Filters',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webmail_plus_preferences_manage_filter'),
    'access callback' => 'user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'webmail_plus.user_preferences_manage_filter.inc',
    'weight' => -9,
  );  

  $items['user/%user/'.WEBMAIL_PLUS_ALIAS.'/filters/manage'] = array(
    'title' => 'Email Filters',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webmail_plus_preferences_manage_filter'),
    'access callback' => 'user_edit_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'file' => 'webmail_plus.user_preferences_manage_filter.inc',
    'weight' => -9,
  );
  
  // show the password tab only if user defined password is enabled
  //if($_webmail_plus_config -> authentication == "user_defined") {
	  $items['user/%user/'.WEBMAIL_PLUS_ALIAS.'/password'] = array(
	    'title' => 'Email Password',
	    'page callback' => 'drupal_get_form',
	    'page arguments' => array('webmail_plus_preferences_password'),
	    'access callback' => 'user_edit_access',
	    'access arguments' => array(1),
	    'type' => MENU_LOCAL_TASK,
	    'file' => 'webmail_plus.user_preferences_password.inc',
	    'weight' => -9,
	  );
  //}
  
  $items['admin/webmail_plus/fix_weight'] = array(
    'title' => 'Adjust module weights to ensure the module functions properly.',
    'access arguments' => array('administer webmail_plus'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webmail_plus_fix_weight'),
    'file' => 'webmail_plus.fix_weight.inc',
    'type' => MENU_CALLBACK,
  );
  
  
  return $items;
}
?>