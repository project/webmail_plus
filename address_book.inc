<?php
/*
note this uses 2 separate forms webmail_plus_address_book_entries and webmail_plus_address_book_new on the same page
it can get tricky so pay attention
*/


function webmail_plus_address_book_form() {
	global $user, $webmail_plus_config;


	$form = array();
	$form['op'] = array(
		'#type' => 'hidden'
	);
	
	// create control buttons that are located on top
	$form['top_buttons']['#tree'] = true;
	
	$form['top_buttons']['compose'] = array(
		'#value' => t('Compose'), 
		'#type' => 'submit',
		
		'#attributes' => array(
			'onclick' => "return webmail_plusAddressbookOpCompose()"
		)
		
	);
	
	$form['top_buttons']['new_entry'] = array(
		'#value' => t('New Entry'), 
		'#type' => 'submit',
		
		'#attributes' => array(
			'onclick' => "return webmail_plusAddressbookOpNew()"
		)		
	);
	
	/*
	$form['top_buttons']['address_book'] = array(
		'#value' => t('Address Book'), 
		'#type' => 'submit'
	);
	*/
	
	//$form['top_buttons']['folders_button'] = array('#value' => 'Folders', '#type' => 'submit');

	// top form
	// show the directory only if LDAP module is enabled
	if(module_exist('ldapdata')) {
		$form['top_buttons']['directory'] = array(
			'#type' => 'submit',
			'#value' => t('View Directory')
		);
	}


	// contents of the actual address book
  	$headers = array(
	    array('data' => t('Select')),
	    array('data' => t('Nickname'), 'field' => 'nickname'),
	    array('data' => t('Email'), 'field' => 'email'),
	    array('data' => t('First Name'), 'field' => 'first_name'),
	    array('data' => t('Last Name'), 'field' => 'last_name'),
	    array('data' => t('Additional Info'), 'field' => 'additional_info')
    );

    // this will get converted to pager query later

    $sql = "SELECT * FROM webmail_plus_address_book WHERE uid=".$user->uid." ".tablesort_sql($headers);

    //echo $sql;

   	$result = pager_query($sql,$webmail_plus_config['messages_per_page'], 0);

    //$result = db_query("SELECT * FROM webmail_plus_address_book WHERE uid=%d", $user->uid);

	$form['entries']['items']['#tree'] = true;	// has to be a tree, otherwise you cant select multiple checkboxes

	$total_items=0;

	while($data = db_fetch_object($result)) {

		//print_r($data->id);

		$form['entries']['items'][$data->id]['id'] = array("#default_value" => '', "#type" => "checkbox", '#default_value' => 0);
		$form['entries']['items'][$data->id]['nickname'] = array("#value" => $data -> nickname);
		$form['entries']['items'][$data->id]['email'] = array("#value" => $data -> email);
		$form['entries']['items'][$data->id]['first_name'] = array("#value" => $data -> first_name);
		$form['entries']['items'][$data->id]['last_name'] = array("#value" => $data -> last_name);
		$form['entries']['items'][$data->id]['additional_info'] = array("#value" => $data -> additional_info);
		$form['entries']['items'][$data->id]['entry_id'] = array('#value' => $data->id, '#type'=>hidden); 

		$total_items++;
	}

	



	if($total_items!="") {

		$form['bottom_buttons']['delete'] = array(
			'#type' => 'submit',
			'#value' => t('Delete'),
			
			'#attributes' => array(
				'onclick' => "return webmail_plusAddressbookOpDelete()"
			)	
		);
	}






	return drupal_get_form('webmail_plus_address_book_form', $form);

	//return $page;
}





function theme_webmail_plus_address_book_form($form) {
	theme('add_style', drupal_get_path('module', 'webmail_plus') . '/webmail_plus.css');

	$output .= form_render($form['top_buttons']);
	unset($form['top_buttons']);

	$output .= form_render($form['new_entry']);
	unset($form['new_entry']);

	$output .= form_render($form['new_entry']);
	unset($form['new_entry']);




	$headers = array(
				array(
				'data'=>'Id',
				'field' =>'id'
				),

				array(
				'data'=>t('Nickname'),
				'field'=>'nickname'
				),

				array(
				'data'=>t('Email'),
				'field'=>'email'
				),

				array(
				'data'=>t('First Name'),
				'field'=>'first_name'
				),

				array(
				'data'=>t('Last Name'),
				'field'=>'last_name'
				),

				array(
				'data'=>t('Additional Info'),
				'field'=>'additional_info'
				),
				
				array(
				'data'=>t('Edit'),
				'field'=>'edit'
				)
	);



	$row = 0;
	foreach (element_children($form['entries']['items']) as $key) {

		$row ++;

		if($row % 2 == 0) {
			$class = 'webmail_plus_row_even';
		} else {
			$class =  'webmail_plus_row_odd';
		}

		// create table rows out of form elements
	    $rows[] = array(
	    	array('data'=>form_render($form['entries']['items'][$key]['id']),'class'=>$class),
	    	array('data'=>form_render($form['entries']['items'][$key]['nickname']),'class'=>$class),

	    	array('data'=>l(form_render($form['entries']['items'][$key]['email']), 'webmail_plus/compose/new/INDEX/0/'.$form['entries']['items'][$key]['email']['#value']),'class'=>$class),

			//array('data'=>form_render($form['entries']['items'][$key]['email']),'class'=>$class),
			array('data'=>form_render($form['entries']['items'][$key]['first_name']),'class'=>$class),
	 		array('data'=>form_render($form['entries']['items'][$key]['last_name']),'class'=>$class),
	 		array('data'=>form_render($form['entries']['items'][$key]['additional_info']),'class'=>$class),
	 		array('data'=>l('Edit', 'webmail_plus/address_book_edit/'.$key),'class'=>$class)
		);


	}




	$output .= theme('table', $headers, $rows);
	unset($form['entries']);

	$output .= theme('pager');

	$output .= "<br>";

	$output .= form_render($form['bottom_buttons']);
	unset($form['bottom_buttons']);

	$output .= form_render($form);	// this is necessary for the form to receive its token

	return $output;
}


function webmail_plus_address_book_form_submit($form_id, $form_values) {
	global $user;


	// __webmail_plus_dump($form_values);
	// die();
	
	// compose button was clicked
	if($form_values == 'compose'){

		if(sizeof($form_values['items'])>0) {

			foreach($form_values['items'] as $id=>$checked) {

				$email = db_result(db_query("SELECT email FROM webmail_plus_address_book WHERE id=%d AND uid=%d", $id, $user->uid));
				$email_list[]=$email;
			}

			$email_string = implode(", ", $email_list);

			//echo $email_string;

			drupal_goto('webmail_plus/compose/new/INDEX/0/'.$email_string);
		} else {
			drupal_goto('webmail_plus/compose');
		}

	}

	if($form_values['op'] == 'new') {
		return 'webmail_plus/address_book_add';
	}


	if($form_values['op'] == 'delete') {
		
		if(sizeof($form_values['items'])>0) {
			foreach($form_values['items'] as $id=>$checked) {
				
				if($checked['id'] == 1) db_query('DELETE FROM webmail_plus_address_book WHERE uid=%d AND id=%d', $user->uid, $id);
				
			}
		}

	}


}






function webmail_plus_address_book_add_form() {
	global $user, $webmail_plus_config;

	$full_email = arg(2);

	$name = __webmail_plus_extract_name($full_email);
	$email = __webmail_plus_extract_email($full_email);

	// if there's a space in the name, it's probably <first name> <last name>
	if(strpos($name, " ")) {
		list($first_name, $last_name) = split(" ", $name);
	} else {
		$first_name = $name;
	}

/*
	$content .= "Email: $email<br>";
	$content .= "First Name: $first_name<br>";
	$content .= "Last Name: $last_name";
	*/

	$form = array();

	
	$form['op'] = array(
		'#type' => 'hidden'
	);
	
	// create control buttons that are located on top
	$form['top_buttons']['#tree'] = true;
	$form['top_buttons']['compose'] = array(
		'#value' => t('Compose'), 
		'#type' => 'submit',
		'#attributes' => array(
			'onclick' => "return webmail_plusAddressbookNewOpCompose()"
		)	
	);
	//$form['top_buttons']['new_entry'] = array('#value' => t('New Entry'), '#type' => 'submit');
	$form['top_buttons']['address_book'] = array(
		'#value' => t('Address Book'), 
		'#type' => 'submit',
		'#attributes' => array(
			'onclick' => "return webmail_plusAddressbookNewOpAddressbook()"
		)	

	);





	// top form
	// show the directory only if LDAP module is enabled
	if(module_exist('ldapdata')) {
		$form['top_buttons']['directory'] = array(
			'#type' => 'submit',
			'#value' => t('View Directory')
		);
	}



	$form['new_entry'] = array(
		'#type' => 'fieldset',
		'#title' => t('New Entry'),
		"#collapsible" => TRUE,
		'#collapsed' => FALSE,
		"#tree" => TRUE

	);


	$form['new_entry']['nickname'] = array(
		'#type' => 'textfield',
		'#title' => t('Nick Name '),
		'#description' => t('Has to be unique.'),
		'#default_value' => $first_name
	);


	$form['new_entry']['email'] = array(
		'#type' => 'textfield',
		'#title' => t('E-mail '),
		'#default_value' => $email
	);

	$form['new_entry']['first_name'] = array(
		'#type' => 'textfield',
		'#title' => t('First Name '),
		'#default_value' => $first_name
	);

	$form['new_entry']['last_name'] = array(
		'#type' => 'textfield',
		'#title' => t('Last Name '),
		'#default_value' => $last_name
	);

	$form['new_entry']['additional_info'] = array(
		'#type' => 'textfield',
		'#title' => t('Additional Info '),
		'#default_value' => $additional_info
		
	);


	$form['new_entry']['add'] = array(
		'#type' => 'submit',
		'#value' => t('Add'),
		'#attributes' => array(
			'onclick' => "return webmail_plusAddressbookNewOpAdd()"
		)			
	);


	return  drupal_get_form('webmail_plus_address_book_add', $form);

}


function webmail_plus_address_book_add_validate($form_id, $form_values) {
	global $user;



	if($form_values['op'] == 'add') {

		// make sure nickname contains only letter and numbers
		if(preg_match("/[^a-z0-9\s\(\)]/i", $form_values['new_entry']['nickname'])) {
			form_set_error('',t('Nickname contains invalid characters. Only letters, numbers, spaces and ( ) are allowed.'));
		}

		// if the nickname is supplied, make sure it's unique
		if(!empty($form_values['new_entry']['nickname'])) {
			//$content.="checking nickname";

			$nickname_used  = db_result(db_query("SELECT COUNT(*) FROM webmail_plus_address_book WHERE uid=%d AND nickname='%s'", $user->uid, $form_values['new_entry']['nickname']));

			if($nickname_used>0)  {
				form_set_error('',t('This nickname is already used.'));
			}
		}



		// make sure that the email address is valid
		if(! __webmail_plus_email_valid(trim($form_values['new_entry']['email']))) {
			form_set_error('',t('Email address is not valid.'));
		}



		// make sure that at least first or last name were entered
		if(empty($form_values['new_entry']['first_name']) && empty($form_values['new_entry']['last_name'])) {
			form_set_error('',t('Name is incomplete. You need to supply at least first or last name.'));
		}
	}

}



function webmail_plus_address_book_add_submit($form_id, $form_values) {
	global $user;

	
/*	__webmail_plus_dump($form_values);
	die();*/
	
	$switch = $form_values['op'];

	switch($switch) {
		case 'add':
			db_query("INSERT INTO {webmail_plus_address_book}(uid,nickname,email,first_name,last_name,additional_info) VALUES(%d,'%s','%s','%s','%s','%s')", $user->uid,strtolower($form_values['new_entry']['nickname']),$form_values['new_entry']['email'],$form_values['new_entry']['first_name'],$form_values['new_entry']['last_name'],$form_values['new_entry']['additional_info']);


			return 'webmail_plus/address_book';
			break;


		case 'compose':
			return 'webmail_plus/compose';
			break;

		case 'addressbook':
			return 'webmail_plus/address_book';
			break;

		default:
			return 'webmail_plus/address_book';
			break;


	}


}

function webmail_plus_address_book_edit_form() {
	global $user, $webmail_plus_config;

	$id = arg(2);

	$rs = db_query("SELECT * FROM webmail_plus_address_book WHERE uid=%d AND id=%d", $user->uid, $id);
	
	$data = db_fetch_object($rs);
	//__webmail_plus_dump($data);
	
	
	//$name = __webmail_plus_extract_name($full_email);
	//$email = __webmail_plus_extract_email($full_email);



	$form = array();

	$form['op'] = array(
		'#type' => 'hidden'
	);
	
	// create control buttons that are located on top
	$form['top_buttons']['#tree'] = true;
	
	$form['top_buttons']['compose'] = array(
		'#value' => t('Compose'), 
		'#type' => 'submit',
		'#attributes' => array(
			'onclick' => "return webmail_plusAddressbookEditOpCompose()"
		)		
	);
	
	//$form['top_buttons']['new_entry'] = array('#value' => t('New Entry'), '#type' => 'submit');
	$form['top_buttons']['address_book'] = array(
		'#value' => t('Address Book'), 
		'#type' => 'submit',
		'#attributes' => array(
			'onclick' => "return webmail_plusAddressbookEditOpAddressbook()"
		)		
	);



	// top form
	// show the directory only if LDAP module is enabled
	if(module_exist('ldapdata')) {
		$form['top_buttons']['directory'] = array(
			'#type' => 'submit',
			'#value' => t('View Directory')
		);
	}



	$form['edit_entry'] = array(
		'#type' => 'fieldset',
		'#title' => t('Editing Existing Entry'),
		"#collapsible" => TRUE,
		'#collapsed' => FALSE,
		"#tree" => TRUE

	);

	$form['edit_entry']['nickname'] = array(
		'#type' => 'textfield',
		'#title' => t('Nick Name '),
		'#description' => t('Has to be unique.'),
		'#default_value' => $data->nickname,
		'#attributes' => array ('disabled' => disabled)
	);


	$form['edit_entry']['email'] = array(
		'#type' => 'textfield',
		'#title' => t('E-mail '),
		'#default_value' => $data->email
	);

	$form['edit_entry']['first_name'] = array(
		'#type' => 'textfield',
		'#title' => t('First Name '),
		'#default_value' => $data->first_name
	);

	$form['edit_entry']['last_name'] = array(
		'#type' => 'textfield',
		'#title' => t('Last Name '),
		'#default_value' => $data->last_name
	);

	$form['edit_entry']['additional_info'] = array(
		'#type' => 'textfield',
		'#title' => t('Additional Info '),
		'#default_value' => $data->additional_info
	);


	$form['edit_entry']['update'] = array(
		'#type' => 'submit',
		'#value' => t('Update'),
		'#attributes' => array(
			'onclick' => "return webmail_plusAddressbookEditOpUpdate()"
		)			
	);

	$form['edit_entry']['id'] = array(
		'#type' => 'hidden',
		'#value' => $data->id
	);
	
	return  drupal_get_form('webmail_plus_address_book_edit', $form);
}


function webmail_plus_address_book_edit_validate($form_id, $form_values) {
	global $user;


	

	if($form_values['op'] ='update') {

		

		// make sure nickname contains only letter and numbers
		if(preg_match("/[^a-z0-9\s\(\)]/i", $form_values['edit_entry']['nickname'])) {
			form_set_error('',t('Nickname contains invalid characters. Only letters, numbers, spaces and ( ) are allowed.'));
		}


		// make sure that the email address is valid
		if(! __webmail_plus_email_valid(trim($form_values['edit_entry']['email']))) {
			form_set_error('',t('Email address is not valid.'));
		}	



		// make sure that at least first or last name were entered
		if(empty($form_values['edit_entry']['first_name']) && empty($form_values['edit_entry']['last_name'])) {
			form_set_error('',t('Name is incomplete. You need to supply at least first or last name.'));
		}
	}
	

}


function webmail_plus_address_book_edit_submit($form_id, $form_values) {
	global $user;

	$switch = $form_values['op'];

	switch($switch) {
		case 'update':
			
			$rs = db_query("UPDATE webmail_plus_address_book SET email='%s', first_name='%s', last_name='%s', additional_info='%s' WHERE uid=%d AND id=%d", $form_values['edit_entry']['email'], $form_values['edit_entry']['first_name'], $form_values['edit_entry']['last_name'],$form_values['edit_entry']['additional_info'],$user->uid, $form_values['edit_entry']['id']);

			return 'webmail_plus/address_book';
			break;

		case 'compose':
			return 'webmail_plus/compose';
			break;


		case 'addressbook':
			return 'webmail_plus/address_book';
			break;


		default:
			return 'webmail_plus/address_book';
			break;


	}


}

?>